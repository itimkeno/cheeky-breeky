#pragma once

#include <game/checkers.hxx>

namespace psc
{

static const std::size_t max_poly_vertex_count = 64;

enum class shape_type
{
    shape_circle,
    shape_polygon,
    shape_AABB,
    shape_count
};

struct shape
{
    virtual ~shape();
    virtual shape*     clone(void) const           = 0;
    virtual void       initialize(void)            = 0;
    virtual void       compute_mass(float density) = 0;
    virtual void       set_orient(float radians)   = 0;
    virtual shape_type get_type(void) const        = 0;

    enum shape_type type;
    // body *body;
    checkers_all::checker* body;

    // circle shape
    float radius = 0.f;
    // polygon shape
    ogl::matrix3x2 matrix_orient{ { 0.f, 0.f }, { 0.f, 0.f }, { 0.f, 0.f } };
};

struct circle : public shape
{
    explicit circle(float r) { radius = r; }
    ~circle() override { delete this; }
    shape*     clone(void) const override;
    void       initialize() override;
    void       compute_mass(float density) override;
    void       set_orient(float radians) override;
    shape_type get_type(void) const override;
};

struct polygon : public shape
{
    explicit polygon() {}
    ~polygon() override { delete this; }
    shape*     clone(void) const override;
    void       initialize(void) override;
    void       compute_mass(float density) override;
    void       set_orient(float radians) override;
    shape_type get_type(void) const override;

    void      set_box(float half_width, float half_height);
    void      set(ogl::vec2* vertices, std::size_t count);
    ogl::vec2 get_support(const ogl::vec2& dir);

    ogl::vec2   m_vertices[max_poly_vertex_count];
    ogl::vec2   m_normals[max_poly_vertex_count];
    std::size_t m_vertex_count = 0;
};

} // namespace psc
