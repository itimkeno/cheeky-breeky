#pragma once

#include <engine/engine.hxx>
#include <game/checkers.hxx>
#include <game/game_object.hxx>

using namespace ogl;

namespace psc
{

// checkers_all::checker == game_object

struct manifold
{
    manifold(checkers_all::checker* a, checkers_all::checker* b)
        : A(a)
        , B(b)
    {
    }

    void initialize(const float);
    void apply_impulse();
    void position_correction();
    void inf_mass_correct(); // infinite mass corrrection
    void solve_collision();

    checkers_all::checker* A;
    checkers_all::checker* B;

    float       penetration; // depth of penetration
    vec2        normal;      // from A to B;
    vec2        contacts[2];
    std::size_t contact_count;
    float       e;  // restitution
    float       df; // friction_dynamic;
    float       sf; // friction_static;
};

// collisions type -------------------------------------------------------------

void circle_to_circle(manifold* m, checkers_all::checker* a,
                      checkers_all::checker* b);
void AABB_to_AABB(manifold* m, checkers_all::checker* a,
                  checkers_all::checker* b);

// collisions type end----------------------------------------------------------
} // namespace psc
