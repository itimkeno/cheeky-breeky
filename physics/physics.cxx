#include "physics.hxx"

#include <cmath>
#include <iostream>

//#include "shape.hxx"

using namespace ogl;

namespace psc
{
// -------- manifold -----------------------------------------------------------
void manifold::solve_collision()
{
    if (A->game_obj.type == object_type::white_checkers &&
        B->game_obj.type == object_type::white_checkers)
        psc::circle_to_circle(this, A, B);

    if (A->game_obj.type == object_type::black_checkers &&
        B->game_obj.type == object_type::black_checkers)
        psc::circle_to_circle(this, A, B);

    if (A->game_obj.type == object_type::white_checkers &&
        B->game_obj.type == object_type::black_checkers)
        psc::circle_to_circle(this, A, B);

    if (A->game_obj.type == object_type::black_checkers &&
        B->game_obj.type == object_type::white_checkers)
        psc::circle_to_circle(this, A, B);

    if (A->game_obj.type == object_type::AABB &&
        B->game_obj.type == object_type::AABB)
        psc::AABB_to_AABB(this, A, B);

#ifndef NDEBUG
    if (A->game_obj.type == object_type::white_checkers &&
        B->game_obj.type == object_type::touch)
        psc::circle_to_circle(this, A, B);
#endif // DEBUG
}

void manifold::initialize(const float dt)
{
    e  = std::min(A->restitution, B->restitution);
    sf = std::sqrt(A->friction_static * B->friction_static);
    df = std::sqrt(A->friction_dynamic * B->friction_dynamic);

    for (std::size_t i = 0; i < contact_count; ++i)
    {
        ogl::vec2 ra = contacts[i] - A->pos;
        ogl::vec2 rb = contacts[i] - B->pos;

        ogl::vec2 relative_velocity =
            B->velocity + cross(B->angular_velocity, rb) - A->velocity -
            cross(A->angular_velocity, ra);

        if (relative_velocity.length_sqr() <
            (dt * ogl::gravity).length_sqr() + ogl::epsilon)
            e = 0.0f;
    }
}
void manifold::apply_impulse()
{
    if (ogl::equal(A->inv_mass + B->inv_mass, 0))
    {
        inf_mass_correct();
        return;
    }

    for (std::size_t i = 0; i < contact_count; i++)
    {
        ogl::vec2 ra = contacts[i] - A->pos;
        ogl::vec2 rb = contacts[i] - B->pos;

        ogl::vec2 relative_velocity =
            B->velocity + cross(B->angular_velocity, rb) - A->velocity -
            cross(A->angular_velocity, ra);

        float contact_velocity = dot(relative_velocity, normal);

        if (contact_velocity > 0)
            return;

        float ra_cross_n   = cross(ra, normal);
        float rb_cross_n   = cross(rb, normal);
        float inv_mass_sum = A->inv_mass + B->inv_mass +
                             ogl::sqr(ra_cross_n) * A->inv_inertia +
                             ogl::sqr(rb_cross_n) * B->inv_inertia;

        // impulse scalar
        float j = -(1.0f + e) * contact_velocity;
        j /= inv_mass_sum;
        j /= static_cast<float>(contact_count);

        // apply impulse
        ogl::vec2 impulse = normal * j;
        A->apply_impulse(-impulse, ra);
        B->apply_impulse(impulse, rb);

        // friction impulse
        relative_velocity = B->velocity + cross(B->angular_velocity, rb) -
                            A->velocity - cross(A->angular_velocity, ra);

        ogl::vec2 t =
            relative_velocity - (normal * dot(relative_velocity, normal));
        t.normalize();

        // impulse tanget magnitude
        float jt = -dot(relative_velocity, t);
        jt /= inv_mass_sum;
        jt /= static_cast<float>(contact_count);

        // don't apply tiny friction impulses
        if (ogl::equal(jt, 0.f))
            return;

        // coulumb's law
        ogl::vec2 tanget_impulse;
        if (std::abs(jt) < j * sf)
        {
            tanget_impulse = t * jt;
        }
        else
        {
            tanget_impulse = t * -j * df;
        }

        // apply friction impulse
        A->apply_impulse(-tanget_impulse, ra);
        B->apply_impulse(tanget_impulse, rb);
    }
}
void manifold::position_correction()
{
    const float penetration_allowance = 0.05f;
    const float percent               = 0.4f; // procentage to correct
    ogl::vec2   correction =
        (std::max(penetration - penetration_allowance, 0.0f) /
         (A->inv_mass + B->inv_mass) * normal * percent);
    A->pos -= correction * A->inv_mass;
    B->pos += correction * B->inv_mass;
}
void manifold::inf_mass_correct()
{
    A->velocity = { 0.f, 0.f };
    B->velocity = { 0.f, 0.f };
}
// -------- manifold end -------------------------------------------------------

// -------- shape --------------------------------------------------------------

// -------- shape end ----------------------------------------------------------

// -------- collision ----------------------------------------------------------
void circle_to_circle(manifold* m, checkers_all::checker* a,
                      checkers_all::checker* b)
{
    ogl::vec2 normal = b->pos - a->pos;

    float distance_sqr = normal.length_sqr();
    float radius_sum   = a->radius + b->radius;

    if (distance_sqr >= radius_sum * radius_sum)
    {
        m->contact_count = 0;
        return;
    }

    float distance = std::sqrt(distance_sqr);

    m->contact_count = 1;

    if (distance == 0.0f)
    {
        m->penetration = a->radius;
        m->normal      = ogl::vec2(1.f, 0.f);
        m->contacts[0] = a->pos;
    }
    else
    {
        m->penetration = radius_sum - distance;
        m->normal      = normal / distance;
        m->contacts[0] = m->normal * a->radius + a->pos;
    }
}

void AABB_to_AABB(manifold* m, checkers_all::checker* a,
                  checkers_all::checker* b)
{
}

// -------- collision end ------------------------------------------------------

} // namespace psc
