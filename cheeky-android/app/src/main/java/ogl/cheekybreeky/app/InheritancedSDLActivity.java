package ogl.cheekybreeky.app;

import org.libsdl.app.SDLActivity;

public class InheritancedSDLActivity extends SDLActivity {

    static {
        System.loadLibrary( "c++_shared" );
        System.loadLibrary( "hidapi" );
        System.loadLibrary(  "SDL2" );
        System.loadLibrary(  "engine" );
        System.loadLibrary(  "game" );
    }
}
