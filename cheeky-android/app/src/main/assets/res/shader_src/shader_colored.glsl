vertex_shader:
#version 300 es
                                  uniform mat3 u_matrix;
        layout(location = 0)      in vec2 a_position;
        layout(location = 1)      in vec4 a_color;
        layout(location = 2)      in vec2 a_tex_coord;
                                  out vec4 v_color;
                                  out vec2 v_tex_coord;
                                  void main()
                                  {
                                      vec3 pos = vec3(a_position, 1.0) * u_matrix;
                                      gl_Position = vec4(pos, 1.0);
                                      v_tex_coord = a_tex_coord;
                                      v_color = a_color;
                                  }

fragment_shader:
#version 300 es
                                  #ifdef GL_ES
                                  precision mediump float;
                                  #endif

                                  in vec4 v_color;
                                  in vec2 v_tex_coord;
                                  out vec4 color;
                                  uniform vec4 u_color;
                                  uniform sampler2D s_texture;
                                  void main()
                                  {
                                      color = texture(s_texture, v_tex_coord) * v_color * u_color;
                                  }

attributes:
{ 0, "a_position" }
{ 1, "a_color" }
{ 2, "a_tex_coord" }
