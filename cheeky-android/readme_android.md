## 1. First make shure your local.properties file in good shape
### Example:
```sh
#ndk.dir=/home/any/Downloads/android-ndk-r18b-linux-x86_64/android-ndk-r18b
ndk.dir=/home/any/Android/Sdk/ndk-bundle
sdk.dir=/home/any/Android/Sdk
sdl2_src.dir=/home/any/Downloads/SDL2-2.0.10
sdl2_build.dir=/home/any/Downloads/SDL2-2.0.10-build
# if you want to use your default system cmake pass prefix to bin/cmake
#cmake.dir=/usr
# if you want to use exact cmake download it from oficial site
#cmake.dir=/home/any/Download/cmake-3.18.0-Linux-x86_64
cmake.dir=/usr
```

## 2. Double check path to cmake see comments in previous example
## 3. Check path to SDL2 source directory
## 4. Add binary dir for every build configuration as we use out of build tree path to SDL2: When specifying an out-of-tree source a binary directory must be explicitly specified.




# how to build android

1. install android sdk
2. install android ndk
3. download sources of SDL2
4. uncompress it to some plase "~/SDL2-2.0.14"
5. create directory            "~/SDL2-2.0.14_build"
6. create local.properties in android-gradle directory example:
```
ndk.dir=/home/any/Android/Sdk/ndk-bundle
sdk.dir=/home/any/Android/Sdk
# If you set this property, Gradle no longer uses PATH to find CMake.
#cmake.dir=/home/any/tools/cmake-3.18.0-Linux-x86_64
sdl2_src.dir=/home/any/SDL2-2.0.14
sdl2_build.dir=/home/any/SDL2-2.0.14_build
```
7. if you want to build arm64v of x86_64 you have to set minSdkVersion 18 - android 4.4
8. add to your build.gradle
```
	    externalNativeBuild {
	        cmake {
                Properties properties = new Properties()
                properties.load(project.rootProject.file('local.properties').newDataInputStream())
                def sdl2SrcDir = properties.getProperty('sdl2_src.dir') ?: "add sdl2_src.dir in local.properties"
                def sdl2BuildDir = properties.getProperty('sdl2_build.dir') ?: "add sdl2_build.dir in local.properties"

                arguments "-DSDL2_SRC_DIR=" + sdl2SrcDir
                arguments "-DSDL2_BUILD_DIR=" + sdl2BuildDir
                arguments "-DANDROID_ARM_NEON=TRUE"
	            arguments "-DANDROID_CPP_FEATURES=rtti exceptions",
	                      "-DANDROID_STL=c++_shared"
	        }
```
