# cheeky-breeky
Alternative cheeckers game

Build platform | Status
---            |---
fedora32       | ![pipeline](https://gitlab.com/itimkeno/cheeky-breeky/badges/master/pipeline.svg?style=flat-square)

Cheeky-breeky project uses a number of open source projects to work properly:

* [SDL2] - best crossplatform game library

 This project is open source with a public repository [cheeky-breeky](https://gitlab.com/itimkeno/cheeky-breeky)
 on GitLab.

## Build on fedora

#### Ddependencies
```sh
  sudo dnf update
  sudo dnf upgrade
  sudo dnf install dnf install gcc-c++ ninja cmake git glibc-static libstdc++-static sdl2 sdl2-devel
```
## Build 
```sh
  git clone https://gitlab.com/itimkeno/cheeky-breeky.git
  cmake -G"Ninja"
  ninja -j 4
```
#### hints on Fedora
1. To search package
```sh
# search by package name
$ dnf search SDL2
# search by file in package
$ dnf provides /usr/lib/libSDL2.so
```
2. To list package contents (files list)
```sh
$ dnf repoquery -l SDL2
$ rpm -ql SDL2
```
3. To show compile/link/version of installed version
```sh
$ sdl2-config --version
$ sdl2-config --libs
$ sdl2-config --static-libs
```

## Build on Windows
#### for Visual Studio (2019 with c++17 support)
1. install vcpkg from (https://github.com/Microsoft/vcpkg)
2. install SDL2 in vcpgk: ```vcpkg install sdl2```
3. make directory build in cheeky-breeky/tests. move into it and there:
```cmake .. -DCMAKE_TOOLCHAIN_FILE={YOUR_DIR}/vcpkg/scripts/buildsystems/vcpkg.cmake```
4. build all using:
```"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe" all-tests-build.sln```

[SDL2]: <http://libsdl.org/>