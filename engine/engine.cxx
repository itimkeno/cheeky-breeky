#include "engine.hxx"

#include <algorithm>
#include <array>
#include <atomic>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string_view>
#include <thread>
#include <tuple>
#include <vector>

#ifdef MINGW
#include <SDL.h>
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#elif __ANDROID__
#include <SDL.h>
#include <android/log.h>
#define GL_GLEXT_PROTOTYPES 1
#include <SDL_opengles2.h>
#define glActiveTexture_ glActiveTexture
#else
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_opengl_glext.h>
#endif

//#include "imgui.h"

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push

// turn off the specific warning. Can also use "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "stb_image.h" // cppcheck-suppress -U
#pragma GCC diagnostic pop

#ifndef __ANDROID__
static PFNGLCREATESHADERPROC             glCreateShader             = nullptr;
static PFNGLSHADERSOURCEPROC             glShaderSource             = nullptr;
static PFNGLCOMPILESHADERPROC            glCompileShader            = nullptr;
static PFNGLGETSHADERIVPROC              glGetShaderiv              = nullptr;
static PFNGLGETSHADERINFOLOGPROC         glGetShaderInfoLog         = nullptr;
static PFNGLDELETESHADERPROC             glDeleteShader             = nullptr;
static PFNGLCREATEPROGRAMPROC            glCreateProgram            = nullptr;
static PFNGLATTACHSHADERPROC             glAttachShader             = nullptr;
static PFNGLBINDATTRIBLOCATIONPROC       glBindAttribLocation       = nullptr;
static PFNGLLINKPROGRAMPROC              glLinkProgram              = nullptr;
static PFNGLGETPROGRAMIVPROC             glGetProgramiv             = nullptr;
static PFNGLGETPROGRAMINFOLOGPROC        glGetProgramInfoLog        = nullptr;
static PFNGLDELETEPROGRAMPROC            glDeleteProgram            = nullptr;
static PFNGLUSEPROGRAMPROC               glUseProgram               = nullptr;
static PFNGLVERTEXATTRIBPOINTERPROC      glVertexAttribPointer      = nullptr;
static PFNGLENABLEVERTEXATTRIBARRAYPROC  glEnableVertexAttribArray  = nullptr;
static PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = nullptr;
static PFNGLVALIDATEPROGRAMPROC          glValidateProgram          = nullptr;
static PFNGLGETUNIFORMLOCATIONPROC       glGetUniformLocation       = nullptr;
static PFNGLUNIFORM1IPROC                glUniform1i                = nullptr;
static PFNGLUNIFORM4FVPROC               glUniform4fv               = nullptr;
static PFNGLACTIVETEXTUREPROC            glActiveTexture_           = nullptr;
static PFNGLGENERATEMIPMAPPROC           glGenerateMipmap           = nullptr;
static PFNGLUNIFORMMATRIX3FVPROC         glUniformMatrix3fv         = nullptr;
static PFNGLBUFFERSUBDATAPROC            glBufferSubData            = nullptr;
static PFNGLBLENDEQUATIONSEPARATEPROC    glBlendEquationSeparate    = nullptr;
static PFNGLBLENDFUNCSEPARATEPROC        glBlendFuncSeparate        = nullptr;
static PFNGLGETATTRIBLOCATIONPROC        glGetAttribLocation        = nullptr;
static PFNGLDELETEBUFFERSPROC            glDeleteBuffers            = nullptr;
static PFNGLDETACHSHADERPROC             glDetachShader             = nullptr;
static PFNGLDELETEVERTEXARRAYSPROC       glDeleteVertexArrays       = nullptr;
static PFNGLBINDBUFFERPROC               glBindBuffer               = nullptr;
static PFNGLGENBUFFERSPROC               glGenBuffers               = nullptr;
static PFNGLGENVERTEXARRAYSPROC          glGenVertexArrays          = nullptr;
static PFNGLBINDVERTEXARRAYPROC          glBindVertexArray          = nullptr;
static PFNGLBUFFERDATAPROC               glBufferData               = nullptr;
#if defined(PFNGLGETERRORPROC)
static PFNGLGETERRORPROC glGetError = nullptr;
#endif
#endif
#ifdef __ANDROID__
static PFNGLGENVERTEXARRAYSOESPROC    glGenVertexArrays    = nullptr;
static PFNGLBINDVERTEXARRAYOESPROC    glBindVertexArray    = nullptr;
static PFNGLDELETEVERTEXARRAYSOESPROC glDeleteVertexArrays = nullptr;
#endif

template <typename T>
static void load_gl_func(const char* func_name, T& result)
{
    void* gl_pointer = SDL_GL_GetProcAddress(func_name);
    if (nullptr == gl_pointer)
    {
        throw std::runtime_error(std::string("can't load GL function ") +
                                 func_name);
    }
    result = reinterpret_cast<T>(gl_pointer);
}

#ifdef DEBUG
#define OMG_CHECK()                                                            \
    {                                                                          \
        const GLenum err = glGetError();                                       \
        if (err != GL_NO_ERROR)                                                \
        {                                                                      \
            switch (err)                                                       \
            {                                                                  \
                case GL_INVALID_ENUM:                                          \
                    std::cerr << "GL_INVALID_ENUM" << std::endl;               \
                    break;                                                     \
                case GL_INVALID_VALUE:                                         \
                    std::cerr << "GL_INVALID_VALUE" << std::endl;              \
                    break;                                                     \
                case GL_INVALID_OPERATION:                                     \
                    std::cerr << "GL_INVALID_OPERATION" << std::endl;          \
                    break;                                                     \
                case GL_INVALID_FRAMEBUFFER_OPERATION:                         \
                    std::cerr << "GL_INVALID_FRAMEBUFFER_OPERATION"            \
                              << std::endl;                                    \
                    break;                                                     \
                case GL_OUT_OF_MEMORY:                                         \
                    std::cerr << "GL_OUT_OF_MEMORY" << std::endl;              \
                    break;                                                     \
            }                                                                  \
            std::cerr << __FILE__ << ':' << __LINE__ << '(' << __FUNCTION__    \
                      << ')' << std::endl;                                     \
            assert(false);                                                     \
        }                                                                      \
    }
#else
#define OMG_CHECK() ;
#endif

namespace ogl
{
// Yes! Global variables!
static SDL_Window*   window     = nullptr;
static SDL_GLContext gl_context = nullptr;

// need add to engine class
static window_mode current_window_mode;

class shader_gl_es30;
static shader_gl_es30* shader_simple  = nullptr;
static shader_gl_es30* shader_colored = nullptr;

static GLuint VBO = 0; // VBO
static GLuint VAO = 0; // VAO

std::ostream&            log(std::cout);
static std::atomic<bool> already_exist = false;

class sound_buffer_impl;
static SDL_AudioDeviceID               audio_device;
static SDL_AudioSpec                   audio_device_spec;
static std::vector<sound_buffer_impl*> sounds;
static void                            audio_callback(void*, uint8_t*, int);

/// imgui -----------
// uint32_t gl_default_vbo   = 0;
// bool     show_demo_window = false;
// static engine* g_engine         = nullptr;

float get_time_from_init()
{
    // ms from library initialization
    std::uint32_t time_init = SDL_GetTicks();
    float         seconds   = time_init * 0.001f;
    return seconds;
}

membuf load_file(std::string_view path)
{
    SDL_RWops* io = SDL_RWFromFile(path.data(), "rb");
    if (nullptr == io)
    {
        throw std::runtime_error("can't load file: " + std::string(path));
    }

    Sint64 file_size = io->size(io);
    if (-1 == file_size)
    {
        throw std::runtime_error("can't determine size of file: " +
                                 std::string(path));
    }
    size_t                  size = static_cast<size_t>(file_size);
    std::unique_ptr<char[]> mem  = std::make_unique<char[]>(size);

    size_t num_readed_objects = io->read(io, mem.get(), size, 1);
    if (num_readed_objects != 1)
    {
        throw std::runtime_error("can't read all content from file: " +
                                 std::string(path));
    }

    if (0 != io->close(io))
    {
        throw std::runtime_error("failed close file: " + std::string(path));
    }
    return membuf(std::move(mem), size);
}

///  math-----------------------------------------------------------
vec2::vec2()
    : x(0.f)
    , y(0.f)
{
}

vec2::vec2(float x_, float y_)
    : x(x_)
    , y(y_)
{
}

float vec2::length() const
{
    return std::sqrt((x * x) + (y * y));
}

float vec2::length_sqr() const
{
    return x * x + y * y;
}

vec2& vec2::normalize()
{
    float loc_length = length();
    if (loc_length > epsilon)
    {
        float inv_length = (1 / loc_length);
        x *= inv_length;
        y *= inv_length;
    }
    return *this;
}

vec2& vec2::rotate(float radians)
{
    //    float xp = x * c - y * s;
    //    float yp = x * s + y * c;
    float xp = x * std::cos(radians) - y * std::sin(radians);
    float yp = x * std::sin(radians) + y * std::sin(radians);

    x = xp;
    y = yp;

    return *this;
}

vec2 min(const vec2& l, const vec2& r)
{
    vec2 result;
    result.x = std::min(l.x, r.x);
    result.y = std::min(l.y, r.y);
    return result;
}

vec2 max(const vec2& l, const vec2& r)
{
    vec2 result;
    result.x = std::max(l.x, r.x);
    result.y = std::max(l.y, r.y);
    return result;
}

float dot(const vec2& l, const vec2& r)
{
    float result;
    result = l.x * r.x + l.y * r.y;
    return result;
}

float dist_sqr(const vec2& l, const vec2& r)
{
    vec2 tmp = l - r;
    return dot(tmp, tmp);
}

vec2 cross(const vec2& l, const float& r)
{
    vec2 result;
    result.x = -r * l.y;
    result.y = r * l.x;
    return result;
}

vec2 cross(const float& l, const vec2& r)
{
    vec2 result;
    result.x = -l * r.y;
    result.y = l * r.x;
    return result;
}

float cross(const vec2& l, const vec2& r)
{
    return l.x * r.y - l.y * r.x;
}
float distance(const vec2& l, const vec2& r)
{
    return std::sqrt(sqr(l.x - r.x) + sqr(l.y - r.y));
}

//---------------self
// impl-- ----------------------------------------------------

bool equal(const float& l, const float& r)
{
    return std::abs(l - r) <= epsilon;
}

float sqr(const float a)
{
    return a * a;
}

float clamp(const float& min, const float& max, const float& a)
{
    if (a < min)
        return min;
    if (a > max)
        return max;
    return a;
}

int round(const float& a)
{
    return static_cast<int>(a + 0.5f);
}

float random(const float& l, const float& r)
{
    float a = static_cast<float>(rand());
    a /= static_cast<float>(RAND_MAX);
    a = (r - l) * a + l;
    return a;
}

bool bias_grater_than(const float& l, const float& r)
{
    const float k_bias_relative = 0.95f;
    const float k_bias_absolute = 0.01f;
    return l >= r * k_bias_relative + l * k_bias_absolute;
}

//------------------------------------------------------------------------------
//  this is work????
//------------------------------------------------------------------------------

vec2 operator+(const vec2& l, const vec2& r)
{
    vec2 result;
    result.x = l.x + r.x;
    result.y = l.y + r.y;
    return result;
}

vec2 operator+(const vec2& l, const float& r)
{
    vec2 result;
    result.x = l.x + r;
    result.y = l.y + r;
    return result;
}

vec2 operator+(const float& l, const vec2& r)
{
    vec2 result;
    result.x = l + r.x;
    result.y = l + r.y;
    return result;
}

vec2 operator-(const vec2& l, const float& r)
{
    vec2 result;
    result.x = l.x - r;
    result.y = l.y - r;
    return result;
}

vec2 operator-(const float& l, const vec2& r)
{
    vec2 result;
    result.x = l - r.x;
    result.y = l - r.y;
    return result;
}

vec2 operator-(const vec2& l, const vec2& r)
{
    vec2 result;
    result.x = l.x - r.x;
    result.y = l.y - r.y;
    return result;
}

vec2 operator*(const vec2& l, const vec2& r)
{
    vec2 result;
    result.x = l.x * r.x;
    result.y = l.y * r.y;
    return result;
}

vec2 operator*(const vec2& l, const float& r)
{
    vec2 result;
    result.x = l.x * r;
    result.y = l.y * r;
    return result;
}

vec2 operator*(const float& l, const vec2& r) // ???
{
    vec2 result;
    result.x = l * r.x;
    result.y = l * r.y;
    return result;
}

vec2& vec2::operator+=(const vec2& r)
{
    x += r.x;
    y += r.y;
    return *this;
}

vec2& vec2::operator-=(const vec2& r)
{
    x -= r.x;
    y -= r.y;
    return *this;
}

vec2& vec2::operator/(const float& r)
{
    x /= r;
    y /= r;
    return *this;
}

vec2 vec2::operator-() const
{
    return vec2(-x, -y);
}

matrix3x2::matrix3x2()
    : row0(1.0f, 0.f)
    , row1(0.f, 1.0f)
    , row2(0.f, 0.f)
{
}

matrix3x2::matrix3x2(vec2 row0_, vec2 row1_, vec2 row2_)
    : row0(row0_)
    , row1(row1_)
    , row2(row2_)
{
}

matrix3x2 matrix3x2::identity()
{
    return matrix3x2::scale(1.f);
}

matrix3x2 matrix3x2::scale(float scale_x, float scale_y)
{
    matrix3x2 result;
    result.row0.x = scale_x;
    result.row1.y = scale_y;
    return result;
}

matrix3x2 matrix3x2::scale(float scale)
{
    matrix3x2 result;
    result.row0.x = scale;
    result.row1.y = scale;
    return result;
}

matrix3x2 matrix3x2::rotation(float thetha)
{
    matrix3x2 result;

    result.row0.x = std::cos(thetha);
    result.row0.y = std::sin(thetha);

    result.row1.x = -std::sin(thetha);
    result.row1.y = std::cos(thetha);

    return result;
}

matrix3x2 matrix3x2::move(const vec2& delta)
{
    matrix3x2 result = matrix3x2::identity();
    result.row2      = delta;

    return result;
}

vec2 matrix3x2::axis_x() const
{
    return vec2(row0.x, row1.x);
}

vec2 matrix3x2::axis_y() const
{
    return vec2(row0.y, row1.y);
}

matrix3x2 matrix3x2::transponse() const
{
    return matrix3x2(axis_x(), axis_y(), this->row2);
}

vec2 operator*(const vec2& v, const matrix3x2& m)
{
    vec2 result;
    result.x = v.x * m.row0.x + v.y * m.row0.y + m.row2.x;
    result.y = v.x * m.row1.x + v.y * m.row1.y + m.row2.y;
    return result;
}

// m1 - left, m2 - right
matrix3x2 operator*(const matrix3x2& m1, const matrix3x2& m2)
{
    matrix3x2 result;

    result.row0.x = m1.row0.x * m2.row0.x + m1.row1.x * m2.row0.y;
    result.row1.x = m1.row0.x * m2.row1.x + m1.row1.x * m2.row1.y;
    result.row0.y = m1.row0.y * m2.row0.x + m1.row1.y * m2.row0.y;
    result.row1.y = m1.row0.y * m2.row1.x + m1.row1.y * m2.row1.y;

    result.row2.x = m1.row2.x * m2.row0.x + m1.row2.y * m2.row0.y + m2.row2.x;
    result.row2.y = m1.row2.x * m2.row1.x + m1.row2.y * m2.row1.y + m2.row2.y;
    return result;

    //      matrix3x2

    // col0   |  col1          | col3  not impl
    //        |
    // row0.x |  row0.y        | 0
    // row1.x |  row1.y        | 0
    // row2.x |  row2.y        | 1
}

/// end math -----------------------------------------------------

static std::ostream& operator<<(std::ostream& out, const SDL_version& v)
{
    out << static_cast<int>(v.major) << '.';
    out << static_cast<int>(v.minor) << '.';
    out << static_cast<int>(v.patch);
    return out;
}

/// geometry -------------------------------------------------
std::istream& operator>>(std::istream& is, matrix3x2& m)
{
    is >> m.row0.x;
    is >> m.row1.x;
    is >> m.row0.y;
    is >> m.row1.y;
    return is;
}

std::istream& operator>>(std::istream& is, vec2& v)
{
    is >> v.x;
    is >> v.y;
    return is;
}

std::istream& operator>>(std::istream& is, color& c)
{
    float r = 0.f;
    float g = 0.f;
    float b = 0.f;
    float a = 0.f;
    is >> r;
    is >> g;
    is >> b;
    is >> a;
    c = color(r, g, b, a);
    return is;
}

std::istream& operator>>(std::istream& is, vertex& v)
{
    is >> v.pos.x;
    is >> v.pos.y;
    is >> v.uv;
    is >> v.c;
    return is;
}
/// end geometry -------------------------------------------------

void resize_window(const int w, const int h)
{
    const float sdl_window_scale = static_cast<float>(w) / h;
    if (sdl_window_scale < current_window_mode.scale)
    {
        int new_h   = w / current_window_mode.scale;
        int shift_y = h - new_h;
        glViewport(0, shift_y, w, new_h);
        OMG_CHECK()
        current_window_mode.set_current_window_mode(w, new_h);
    }
    else
    {
        int new_w = h * current_window_mode.scale;
        glViewport(0, 0, new_w, h);
        OMG_CHECK()
        current_window_mode.set_current_window_mode(new_w, h);
    }
}

void window_mode::set_current_window_mode(const int width_, const int height_)
{
    width  = width_;
    height = height_;
    scale  = (width == 0.0 || height == 0.0)
                 ? 1.f
                 : (static_cast<float>(width) / static_cast<float>(height));
}

window_mode window_mode::get_current_window_mode()
{
    return window_mode{ current_window_mode.width, current_window_mode.height,
                        current_window_mode.scale, false };
}

float window_mode::get_window_scale()
{
    if (width == 0 || height == 0)
    {
        return 1.f;
    }
    scale = static_cast<float>(width) / static_cast<float>(height);
    return scale;
}

/// vertex buffer---------------------------------------------
class vertex_buffer_impl final : public vertex_buffer
{
public:
    vertex_buffer_impl(const vertex* tri, std::size_t n)
        : vertexes(n)
    {
        assert(tri != nullptr);

        glGenBuffers(1, &gl_handle);
        OMG_CHECK()

        bind();

        GLsizeiptr size_int_bytes = static_cast<GLsizeiptr>(n * sizeof(vertex));
        glBufferData(GL_ARRAY_BUFFER, size_int_bytes, tri, GL_STATIC_DRAW);
        OMG_CHECK()
    }
    ~vertex_buffer_impl() final
    {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        OMG_CHECK()
        glDeleteBuffers(1, &gl_handle);
        OMG_CHECK()
    }

    void bind() const override
    {
        glBindBuffer(GL_ARRAY_BUFFER, gl_handle);
        OMG_CHECK()
    }

    std::size_t size() const override final { return vertexes; }

private:
    GLuint vertexes  = 0;
    GLuint gl_handle = 0;
};

vertex_buffer* create_vertex_buffer(const vertex* vertexes, std::size_t n)
{
    return new vertex_buffer_impl(vertexes, n);
}
void destroy_vertex_buffer(vertex_buffer* buffer)
{
    delete buffer;
}

vertex_buffer::~vertex_buffer()
{
    // delete this;
}
/// vertex buffer end --------------------------------------
///
/// index buffer--------------------------------------------

////-index buffer end--------------------------------------------------------

/// sound ----------------------------------------------------------
static std::string_view get_sound_format_name(uint16_t format_value)
{
    static const std::map<int, std::string_view> format = {
        { AUDIO_U8, "AUDIO_U8" },         { AUDIO_S8, "AUDIO_S8" },
        { AUDIO_U16LSB, "AUDIO_U16LSB" }, { AUDIO_S16LSB, "AUDIO_S16LSB" },
        { AUDIO_U16MSB, "AUDIO_U16MSB" }, { AUDIO_S16MSB, "AUDIO_S16MSB" },
        { AUDIO_S32LSB, "AUDIO_S32LSB" }, { AUDIO_S32MSB, "AUDIO_S32MSB" },
        { AUDIO_F32LSB, "AUDIO_F32LSB" }, { AUDIO_F32MSB, "AUDIO_F32MSB" },
    };

    auto it = format.find(format_value);
    return it->second;
}

static std::size_t get_sound_format_size(uint16_t format_value)
{
    static const std::map<int, std::size_t> format = {
        { AUDIO_U8, 1 },     { AUDIO_S8, 1 },     { AUDIO_U16LSB, 2 },
        { AUDIO_S16LSB, 2 }, { AUDIO_U16MSB, 2 }, { AUDIO_S16MSB, 2 },
        { AUDIO_S32LSB, 4 }, { AUDIO_S32MSB, 4 }, { AUDIO_F32LSB, 4 },
        { AUDIO_F32MSB, 4 },
    };

    auto it = format.find(format_value);
    return it->second;
}

#pragma pack(push, 2)
class sound_buffer_impl final : public sound_buffer
{
public:
    sound_buffer_impl(std::string_view path, SDL_AudioDeviceID device,
                      SDL_AudioSpec audio_spec);
    ~sound_buffer_impl() final;

    void play(const properties prop) final
    {
        SDL_LockAudioDevice(device);

        current_index = 0;
        is_playing_   = true;
        is_looped     = (prop == properties::looped);

        SDL_UnlockAudioDevice(device);
    }
    bool is_playing() const final { return is_playing_; }
    void stop() final
    {
        SDL_LockAudioDevice(device);

        current_index = 0;
        is_playing_   = false;
        is_looped     = false;

        SDL_UnlockAudioDevice(device);
    }
    std::unique_ptr<uint8_t[]> tmp_buf;
    uint8_t*                   buffer;
    uint32_t                   length;
    uint32_t                   current_index = 0;
    SDL_AudioDeviceID          device;
    bool                       is_playing_ = false;
    bool                       is_looped   = false;
};
#pragma pack(pop)

sound_buffer_impl::sound_buffer_impl(std::string_view  path,
                                     SDL_AudioDeviceID device_,
                                     SDL_AudioSpec     device_audio_spec)
    : buffer(nullptr)
    , length(0)
    , device(device_)
{
    SDL_RWops* file = SDL_RWFromFile(path.data(), "rb");
    if (file == nullptr)
    {
        throw std::runtime_error(std::string("can't open audio file: ") +
                                 path.data());
    }

    // freq, format, channels, and samples - used by SDL_LoadWAV_RW
    SDL_AudioSpec file_audio_spec;

    if (nullptr == SDL_LoadWAV_RW(file, 1, &file_audio_spec, &buffer, &length))
    {
        throw std::runtime_error(std::string("can't load wav: ") + path.data());
    }

    ogl::log << "audio format for: " << path << '\n'
             << "format: " << get_sound_format_name(file_audio_spec.format)
             << '\n'
             << "sample_size: " << get_sound_format_size(file_audio_spec.format)
             << '\n'
             << "channels: " << static_cast<uint32_t>(file_audio_spec.channels)
             << '\n'
             << "frequency: " << file_audio_spec.freq << '\n'
             << "length: " << length << '\n'
             << "time: "
             << static_cast<double>(length) /
                    (static_cast<uint32_t>(file_audio_spec.channels) *
                     static_cast<uint32_t>(file_audio_spec.freq) *
                     get_sound_format_size(file_audio_spec.format))
             << "sec" << std::endl;

    if (file_audio_spec.channels != device_audio_spec.channels ||
        file_audio_spec.format != device_audio_spec.format ||
        file_audio_spec.freq != device_audio_spec.freq)
    {
        SDL_AudioCVT cvt;
        SDL_BuildAudioCVT(&cvt, file_audio_spec.format,
                          file_audio_spec.channels, file_audio_spec.freq,
                          device_audio_spec.format, device_audio_spec.channels,
                          device_audio_spec.freq);
        assert(cvt.needed); // obviously, this one is always needed.
        // read your data into cvt.buf here.
        cvt.len = static_cast<int>(length);
        // we have to make buffer for inplace conversion
        std::size_t new_buffer_size =
            static_cast<std::size_t>(cvt.len) * cvt.len_mult;
        tmp_buf.reset(new uint8_t[new_buffer_size]);
        uint8_t* buf = tmp_buf.get();
        std::copy_n(buffer, length, buf);
        cvt.buf = buf;
        if (0 != SDL_ConvertAudio(&cvt))
        {
            ogl::log << "failed to convert audio from file: " << path
                     << " to audio device format" << std::endl;
        }
        // cvt.buf has cvt.len_cvt bytes of converted data now.
        SDL_FreeWAV(buffer);
        buffer = tmp_buf.get();
        length = static_cast<uint32_t>(cvt.len_cvt);
    }
}

sound_buffer_impl::~sound_buffer_impl()
{
    if (!tmp_buf)
    {
        SDL_FreeWAV(buffer);
    }
    buffer = nullptr;
    length = 0;
    // delete this;
}

sound_buffer* create_sound_buffer(std::string_view path)
{
    SDL_LockAudioDevice(audio_device); // TODO fix lock only push_back
    sound_buffer_impl* s =
        new sound_buffer_impl(path, audio_device, audio_device_spec);
    sounds.push_back(s);
    SDL_UnlockAudioDevice(audio_device);
    return s;
}

sound_buffer::~sound_buffer() {}

void destroy_sound_buffer(sound_buffer* sound)
{
    // TODO FIXME first remove from sounds collection
    delete sound;
}

/// sound end --------------------------------------------------------

#pragma pack(push, 4)
class texture_gl_es20 final : public texture
{
public:
    explicit texture_gl_es20(std::string_view path)
        : file_path(path)
    {
        membuf file_contents = load_file(path);

        stbi_set_flip_vertically_on_load(true);
        int w          = 0;
        int h          = 0;
        int components = 0;

        unsigned char* decoded_img = stbi_load_from_memory(
            reinterpret_cast<unsigned char*>(file_contents.begin()),
            file_contents.size(), &w, &h, &components, 4);

        if (decoded_img == nullptr)
        {
            std::cerr << "error: " << path << std::endl;
            throw std::runtime_error("can't load texture stbi");
        }

        generate_texture(decoded_img, static_cast<size_t>(w),
                         static_cast<size_t>(h));

        free(decoded_img);

        this->width  = static_cast<uint32_t>(w);
        this->height = static_cast<uint32_t>(h);
    }
    texture_gl_es20(const void* pixels, const size_t w, const size_t h)
        : width(w)
        , height(h)
    {
        generate_texture(pixels, w, h);
        if (file_path.empty())
        {
            file_path = "::memory::";
        }
    }

    ~texture_gl_es20() override
    {
        glDeleteTextures(1, &tex_handl);
        OMG_CHECK()
    }

    void bind() const
    {
        GLboolean is_texture = glIsTexture(tex_handl);
        SDL_assert(is_texture);
        OMG_CHECK()
        glBindTexture(GL_TEXTURE_2D, tex_handl);
        OMG_CHECK()
    }

    std::uint32_t get_width() const final { return width; }
    std::uint32_t get_height() const final { return height; }
    std::string   get_name() const override { return file_path; }

    void add_ref() { ++ref_count; }
    bool remove_ref()
    {
        --ref_count;
        if (ref_count == 0)
        {
            delete this;
            return true;
        }
        return false;
    }

private:
    void generate_texture(const void* pixels, const std::size_t w,
                          const std::size_t h)
    {
        glGenTextures(1, &tex_handl);
        OMG_CHECK()
        glBindTexture(GL_TEXTURE_2D, tex_handl);
        OMG_CHECK()

        GLint   mipmap_level = 0;
        GLint   border       = 0;
        GLsizei width_       = static_cast<GLsizei>(w);
        GLsizei height_      = static_cast<GLsizei>(h);

        glTexImage2D(GL_TEXTURE_2D, mipmap_level, GL_RGBA, width_, height_,
                     border, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
        OMG_CHECK()

        glGenerateMipmap(GL_TEXTURE_2D);
        OMG_CHECK()

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        OMG_CHECK()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        OMG_CHECK()
    }

    std::string   file_path;
    GLuint        tex_handl = 0;
    std::uint32_t width     = 0;
    std::uint32_t height    = 0;
    size_t        ref_count = 1;
};
#pragma pack(pop)

texture* create_texture(std::string_view path)
{
    return new texture_gl_es20(path);
}
void destroy_texture(texture* t)
{
    delete t;
}
texture::~texture() {}

class shader_gl_es30
{
public:
    shader_gl_es30(
        std::string_view vertex_src, std::string_view fragment_src,
        const std::vector<std::tuple<GLuint, const GLchar*>>& attributes)
    {
        vertex_shader   = compile_shader(GL_VERTEX_SHADER, vertex_src);
        fragment_shader = compile_shader(GL_FRAGMENT_SHADER, fragment_src);
        if (vertex_shader == 0 || fragment_shader == 0)
        {
            throw std::runtime_error("can'tt compile shader ");
        }
        program_id = link_shader_program(attributes);
        if (program_id == 0)
        {
            throw std::runtime_error("cant link shader ");
        }
    }

    explicit shader_gl_es30(std::string_view shader_file)
    {
        std::string                                    vertex_src;
        std::string                                    fragment_src;
        std::vector<std::tuple<GLuint, const GLchar*>> attributes;

        std::stringstream shaders_src = filter_comments(shader_file);
        if (!shaders_src)
        {
            std::cerr << "can't load shaders data from " << shader_file.data()
                      << std::endl;
        }

        constexpr auto vertex_mark     = "vertex_shader:";
        constexpr auto fragment_mark   = "fragment_shader:";
        constexpr auto attributes_mark = "attributes:";

        std::string line;

        std::getline(shaders_src, line);
        if (line.substr(0, strlen(vertex_mark)) != vertex_mark)
        {
            std::cerr << "wrong format in " << shader_file.data()
                      << " file. Can't find vertex shader sources."
                      << std::endl;
        }

        while (std::getline(shaders_src, line))
        {
            if (line.substr(0, strlen(fragment_mark)) == fragment_mark)
                break;

            vertex_src += line + '\n';
        }

        if (line.substr(0, strlen(fragment_mark)) != fragment_mark)
        {
            std::cerr << "wrong format in " << shader_file.data()
                      << " file. Can't find fragment shader sources."
                      << std::endl;
        }

        while (std::getline(shaders_src, line))
        {
            if (line.substr(0, strlen(attributes_mark)) == attributes_mark)
                break;

            fragment_src += line + '\n';
        }

        while (getline(shaders_src, line))
        {
            size_t pos_beg = line.find_first_of("0123456789");
            size_t pos_end = line.find(',');

            GLuint attr_idx = stoi(line.substr(pos_beg, (pos_end - pos_beg)));

            pos_beg = line.find('"') + 1;
            pos_end = line.find_last_of('"');

            std::string attr_name = line.substr(pos_beg, (pos_end - pos_beg));
            size_t      len       = attr_name.length();
            char*       res       = (char*)malloc(sizeof(char) * (len + 1));
            strncpy(res, attr_name.c_str(), sizeof(char) * len);

            attributes.push_back(std::tuple(attr_idx, res));
        }

        vertex_shader   = compile_shader(GL_VERTEX_SHADER, vertex_src);
        fragment_shader = compile_shader(GL_FRAGMENT_SHADER, fragment_src);
        if (vertex_shader == 0 || fragment_shader == 0)
        {
            throw std::runtime_error("can'tt compile shader ");
        }
        program_id = link_shader_program(attributes);
        if (program_id == 0)
        {
            throw std::runtime_error("cant link shader ");
        }
    }

    ~shader_gl_es30()
    {
        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);
    }

    void use() const
    {
        glUseProgram(program_id);
        OMG_CHECK()
    }

    void set_uniform(std::string_view       uniform_name,
                     const texture_gl_es20* texture)
    {
        assert(texture != nullptr);
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        OMG_CHECK()
        if (location == -1)
        {
            std::cerr << "can't get uniform_texture location from shader \n";
            throw std::runtime_error("cant get uniform location ");
        }
        unsigned int texture_unit = 0;
        glActiveTexture_(GL_TEXTURE0 + texture_unit);
        OMG_CHECK()

        texture->bind();

        glUniform1i(location, static_cast<int>(0 + texture_unit));
        OMG_CHECK()
    }

    void set_uniform(std::string_view uniform_name, const color& c)
    {
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        OMG_CHECK()
        if (location == -1)
        {
            std::cerr << "can't get uniform_color location from shader \n";
            throw std::runtime_error("can't get uniform location ");
        }

        float values[4] = { c.get_r(), c.get_g(), c.get_b(), c.get_a() };

        glUniform4fv(location, 1, &values[0]);
        OMG_CHECK()
    }

    void set_uniform(std::string_view uniform_name, const matrix3x2& m)
    {
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        OMG_CHECK()
        if (location == -1)
        {
            std::cerr << "can't get uniform location from shader: "
                      << uniform_name.data() << std::endl;
            throw std::runtime_error("can't get uniform location");
        }

        // clang-format off
        float values[9] = {  m.row0.x,  m.row0.y,  m.row2.x,
                             m.row1.x,  m.row1.y,  m.row2.y,
                             0.f,       0.f,       1.f };
        // clang-format on

        glUniformMatrix3fv(location, 1, GL_FALSE, &values[0]);
        OMG_CHECK()
    }

    void set_uniform_imgui(std::string_view uniform_name, const matrix3x2& m)
    {
        const int location =
            glGetUniformLocation(program_id, uniform_name.data());
        OMG_CHECK()
        if (location == -1)
        {
            std::cerr << "can't get uniform location from shader: "
                      << uniform_name.data() << std::endl;
            throw std::runtime_error("can't get uniform location");
        }

        // clang-format off
        float values[9] = {  m.row0.x,  m.row0.y,  0.f,
                             m.row1.x,  m.row1.y,  0.f,
                             m.row2.x,  m.row2.y,  1.f };
        // clang-format on

        glUniformMatrix3fv(location, 1, GL_FALSE, &values[0]);
        OMG_CHECK()
    }

private:
    GLuint compile_shader(GLenum shader_type, std::string_view src)
    {
        GLuint shader_id = glCreateShader(shader_type);
        OMG_CHECK()
        std::string_view shader_src = src;
        const char*      source     = shader_src.data();
        glShaderSource(shader_id, 1, &source, nullptr);
        OMG_CHECK()
        glCompileShader(shader_id);
        OMG_CHECK()

        GLint compiled_status = 0;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compiled_status);
        OMG_CHECK()
        if (compiled_status == 0)
        {
            GLint info_len = 0;
            glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &info_len);
            OMG_CHECK()
            std::vector<char> info_chars(static_cast<std::size_t>(info_len));
            glGetShaderInfoLog(shader_id, info_len, nullptr, info_chars.data());
            OMG_CHECK()
            glDeleteShader(shader_id);
            OMG_CHECK()

            std::string shader_type_name =
                shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment";
            std::cerr << "Error compiling shader(" << shader_type_name << ")\n"
                      << shader_src << "\n"
                      << info_chars.data();
            return 0;
        }
        return shader_id;
    }
    GLuint link_shader_program(
        const std::vector<std::tuple<GLuint, const GLchar*>>& attributes)
    {
        GLuint program_id_ = glCreateProgram();
        OMG_CHECK()
        if (0 == program_id_)
        {
            std::cerr << "failed to create gl program";
            throw std::runtime_error("can't link shader");
        }

        glAttachShader(program_id_, vertex_shader);
        OMG_CHECK()
        glAttachShader(program_id_, fragment_shader);
        OMG_CHECK()

        // bind attr location
        for (const auto& attr : attributes)
        {
            GLuint        loc  = std::get<GLuint>(attr);
            const GLchar* name = std::get<const GLchar*>(attr);
            glBindAttribLocation(program_id_, loc, name);
            OMG_CHECK()
        }

        glLinkProgram(program_id_);
        OMG_CHECK()
        GLint linked_status = 0;
        glGetProgramiv(program_id_, GL_LINK_STATUS, &linked_status);
        OMG_CHECK()
        if (linked_status == 0)
        {
            GLint infoLen = 0;
            glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &infoLen);
            OMG_CHECK()
            std::vector<char> infoLog(static_cast<std::size_t>(infoLen));
            glGetProgramInfoLog(program_id_, infoLen, nullptr, infoLog.data());
            OMG_CHECK()
            std::cerr << "Error linking program:\n" << infoLog.data();
            glDeleteProgram(program_id_);
            OMG_CHECK();
            return 0;
        }
        return program_id_;
    }

    GLuint vertex_shader   = 0;
    GLuint fragment_shader = 0;
    GLuint program_id      = 0;
};

/// input ------------------------------------------------------- input

std::ostream& operator<<(std::ostream& stream, const input_data_keyboard& i)
{
    static const std::array<std::string_view, 10> key_names = {
        { "escape", "tab", "space", "left", "right", "up", "down", "i",
          "button_right", "button_left" }
    };

    const std::string_view& key_name =
        key_names[static_cast<std::size_t>(i.key)];

    stream << "key: " << key_name << " is_down: " << i.is_down << std::endl;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const input_data_mouse& i)
{
    static const std::array<std::string_view, 2> key_names = { "mouse_right",
                                                               "mouse_left" };
    const std::string_view&                      key_name =
        key_names[static_cast<std::size_t>(i.key_mouse)];
    stream << "button " << key_name << "is_down: " << i.is_down_button
           << " x: " << i.x << "y: " << i.y << std::endl
           << "button " << key_name << "is_up: " << i.is_up_button
           << " x: " << i.x << "y: " << i.y << std::endl;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const mouse_motion_data& i)
{
    stream << "is moving " << i.x << ' ' << i.y
           << " state right: " << i.right_state
           << " state left: " << i.left_state << std::endl;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const hardware_data& h)
{
    stream << "reset console: " << h.is_reset;
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const event e)
{
    switch (e.type)
    {
        case ogl::event_type::input_key:
            stream << std::get<ogl::input_data_keyboard>(e.info);
            break;
        case ogl::event_type::hardware:
            stream << std::get<ogl::hardware_data>(e.info);
            break;
        case ogl::event_type::mouse_button:
            stream << std::get<ogl::input_data_mouse>(e.info);
            break;
        case ogl::event_type::mouse_motion:
            stream << std::get<ogl::mouse_motion_data>(e.info);
            break;
    }
    return stream;
}

//#pragma pack(push, 1)
struct bind
{
    bind(std::string_view s, SDL_Keycode k, ekeys ogl_k)
        : name(s)
        , key(k)
        , ogl_key(ogl_k)
    {
    }
    std::string_view name;
    SDL_Keycode      key;

    ogl::ekeys ogl_key;
};
//#pragma pack(pop)

const std::array<bind, 10> keys{
    { bind{ "exit", SDLK_ESCAPE, ekeys::escape },
      bind{ "change", SDLK_TAB, ekeys::tab },
      bind{ "jump", SDLK_SPACE, ekeys::space },
      bind{ "left", SDLK_LEFT, ekeys::left },
      bind{ "right", SDLK_RIGHT, ekeys::right },
      bind{ "up", SDLK_UP, ekeys::up }, bind{ "down", SDLK_DOWN, ekeys::down },
      bind{ "imgui", SDLK_i, ekeys::i },
      bind{ "button_right", SDLK_k, ekeys::button_right },
      bind{ "button_left", SDLK_l, ekeys::button_left } }
};

static bool chek_input(const SDL_Event& e, const bind*& result)
{
    const auto it =
        std::find_if(begin(keys), end(keys),
                     [&](const bind& b) { return b.key == e.key.keysym.sym; });

    if (it != end(keys))
    {
        result = &(*it);
        return true;
    }
    return false;
}

/// end input ------------------------------------------------ end input

bool read_event(event& e)
{
    SDL_Event sdl_event{};

    while (SDL_PollEvent(&sdl_event))
    {
        const bind* binding_key = nullptr;
        if (sdl_event.type == SDL_QUIT)
        {
            e.info      = ogl::hardware_data{ true, false };
            e.timestamp = sdl_event.common.timestamp * 0.001;
            e.type      = ogl::event_type::hardware;
            return true;
        }
        else if (sdl_event.type == SDL_KEYDOWN || sdl_event.type == SDL_KEYUP)
        {
            if (chek_input(sdl_event, binding_key))
            {
                bool is_down = sdl_event.type == SDL_KEYDOWN;
                e.info =
                    ogl::input_data_keyboard{ binding_key->ogl_key, is_down };
                e.timestamp = sdl_event.common.timestamp * 0.001;
                e.type      = ogl::event_type::input_key;
                return true;
            }
        }
        else if (sdl_event.type == SDL_MOUSEBUTTONDOWN ||
                 sdl_event.type == SDL_MOUSEBUTTONUP)
        {
            if (sdl_event.button.button == SDL_BUTTON_MIDDLE)
            {
                return false;
            }

            // int w = 0;
            // int h = 0;
            // SDL_GetWindowSize(window, &w, &h);
            bool is_down     = SDL_MOUSEBUTTONDOWN == sdl_event.type;
            bool is_up       = SDL_MOUSEBUTTONUP == sdl_event.type;
            int  x_click_pos = sdl_event.button.x;
            int  y_click_pos = sdl_event.button.y;
            if (sdl_event.type == SDL_MOUSEBUTTONDOWN)
            {
                if (sdl_event.button.button == SDL_BUTTON_RIGHT)
                {
                    enum mouse button = mouse::button_right;

                    e.info      = ogl::input_data_mouse{ button, is_down, is_up,
                                                    x_click_pos, y_click_pos };
                    e.timestamp = sdl_event.common.timestamp * 0.001;
                    e.type      = ogl::event_type::mouse_button;
                }
                if (sdl_event.button.button == SDL_BUTTON_LEFT)
                {
                    enum mouse button = mouse::button_left;

                    e.info      = ogl::input_data_mouse{ button, is_down, is_up,
                                                    x_click_pos, y_click_pos };
                    e.timestamp = sdl_event.common.timestamp * 0.001;
                    e.type      = ogl::event_type::mouse_button;
                }
                return true;
            }
            if (sdl_event.type == SDL_MOUSEBUTTONUP)
            {
                if (sdl_event.button.button == SDL_BUTTON_RIGHT)
                {
                    enum mouse button = mouse::button_right;

                    e.info      = ogl::input_data_mouse{ button, is_down, is_up,
                                                    x_click_pos, y_click_pos };
                    e.timestamp = sdl_event.common.timestamp * 0.001;
                    e.type      = ogl::event_type::mouse_button;
                }
                if (sdl_event.button.button == SDL_BUTTON_LEFT)
                {
                    enum mouse button = mouse::button_left;

                    e.info      = ogl::input_data_mouse{ button, is_down, is_up,
                                                    x_click_pos, y_click_pos };
                    e.timestamp = sdl_event.common.timestamp * 0.001;
                    e.type      = ogl::event_type::mouse_button;
                }
                return true;
            }
        } //  fix this
        if (sdl_event.motion.type == SDL_MOUSEMOTION)
        {
            int  x_pos       = sdl_event.motion.x;
            int  y_pos       = sdl_event.motion.y;
            bool left_state  = false;
            bool right_state = false;

            if (sdl_event.motion.state == SDL_BUTTON_RMASK)
            {
                right_state = true;
            }
            if (sdl_event.motion.state == SDL_BUTTON_LMASK)
            {
                left_state = true;
            }

            e.info = ogl::mouse_motion_data{ true, x_pos, y_pos, left_state,
                                             right_state };
            e.timestamp = sdl_event.common.timestamp * 0.001;
            e.type      = ogl::event_type::mouse_motion;
            return true;
        }
        // =-=--=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=
        else if (sdl_event.type == SDL_WINDOWEVENT)
        {
            if (sdl_event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
            {
                int width  = sdl_event.window.data1;
                int height = sdl_event.window.data2;
                resize_window(width, height);

                e.info      = ogl::hardware_data{ false, true };
                e.timestamp = sdl_event.common.timestamp * 0.001;
                e.type      = ogl::event_type::hardware;
                return true;
            }
        }
    }
    return false;
}

bool is_key_down(const enum ekeys key)
{
    const auto it =
        std::find_if(begin(keys), end(keys),
                     [&](const bind& b) { return b.ogl_key == key; });

    if (it != end(keys))
    {
        const std::uint8_t* state         = SDL_GetKeyboardState(nullptr);
        int                 sdl_scan_code = SDL_GetScancodeFromKey(it->key);
        return state[sdl_scan_code];
    }
    return false;
}

static const std::array<GLenum, 6> primitive_types = {
    GL_LINES,     GL_LINE_STRIP,     GL_LINE_LOOP,
    GL_TRIANGLES, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN
};

void render(const primitives primitive_type, const vertex_buffer& buff,
            const texture* tex, const matrix3x2& m)
{
    shader_simple->use();
    const texture_gl_es20* texture = static_cast<const texture_gl_es20*>(tex);
    texture->bind();
    shader_simple->set_uniform("s_texture", texture);
    shader_simple->set_uniform("u_matrix", m);

    buff.bind();

    // pos
    glEnableVertexAttribArray(0);
    OMG_CHECK()
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), nullptr);
    OMG_CHECK()

    // color
    glEnableVertexAttribArray(1);
    OMG_CHECK()
    glVertexAttribPointer(
        1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex),
        reinterpret_cast<void*>(sizeof(vertex::pos) + sizeof(vertex::uv)));
    OMG_CHECK()

    // texture
    glEnableVertexAttribArray(2);
    OMG_CHECK()
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void*>(sizeof(vertex::pos)));
    OMG_CHECK()

    GLsizei num_of_vertexes = static_cast<GLsizei>(buff.size());
    GLenum  prim_type = primitive_types[static_cast<uint32_t>(primitive_type)];
    glDrawArrays(prim_type, 0, num_of_vertexes);
    OMG_CHECK()

    glDisableVertexAttribArray(1);
    OMG_CHECK()
    glDisableVertexAttribArray(2);
    OMG_CHECK()
}

void render_color(const primitives primitive_type, const vertex_buffer& buff,
                  const texture* tex, const matrix3x2& m, const color& col)
{
    shader_colored->use();
    const texture_gl_es20* texture = static_cast<const texture_gl_es20*>(tex);
    texture->bind();
    shader_colored->set_uniform("s_texture", texture);
    shader_colored->set_uniform("u_matrix", m);
    shader_colored->set_uniform("u_color", col);

    buff.bind();

    // pos
    glEnableVertexAttribArray(0);
    OMG_CHECK()
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex), nullptr);
    OMG_CHECK()

    // color
    glEnableVertexAttribArray(1);
    OMG_CHECK()
    glVertexAttribPointer(
        1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex),
        reinterpret_cast<void*>(sizeof(vertex::pos) + sizeof(vertex::uv)));
    OMG_CHECK()

    // texture
    glEnableVertexAttribArray(2);
    OMG_CHECK()
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(vertex),
                          reinterpret_cast<void*>(sizeof(vertex::pos)));
    OMG_CHECK()

    GLsizei num_of_vertexes = static_cast<GLsizei>(buff.size());
    GLenum  prim_type = primitive_types[static_cast<uint32_t>(primitive_type)];
    glDrawArrays(prim_type, 0, num_of_vertexes);
    OMG_CHECK()

    glDisableVertexAttribArray(1);
    OMG_CHECK()
    glDisableVertexAttribArray(2);
    OMG_CHECK()
}

/*void engine::imgui_draw(const matrix3x2& m)
{
    ImGui_ImplSdlGL3_NewFrame(window);

    if (show_demo_window)
    {
        ImGui::ShowDemoWindow(&show_demo_window);
    }

    if (ImGui::Button("Position"))
    {
        std::cout << m.row0.x << ' ' << m.row0.y << std::endl;
        std::cout << m.row1.x << ' ' << m.row1.y << std::endl;
        std::cout << m.row2.x << ' ' << m.row2.y << std::endl;
        std::cout << "-------------------------" << std::endl;
    }

    ImGui::Render();
    // ImGui_ImplSdlGL3_RenderDrawLists(ImGui::GetDrawData());
}*/

void swap_buffers()
{
    SDL_GL_SwapWindow(window);

    glClear(GL_COLOR_BUFFER_BIT);
    OMG_CHECK()
}

color::color(std::uint32_t rgba_)
    : rgba(rgba_)
{
}

color::color(float r, float g, float b, float a)
{
    assert(r <= 1 && r >= 0);
    assert(g <= 1 && g >= 0);
    assert(b <= 1 && b >= 0);
    assert(a <= 1 && a >= 0);

    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);

    rgba = a_ << 24 | b_ << 16 | g_ << 8 | r_;
}

// get_color()
float color::get_r() const
{
    std::uint32_t r_ = (rgba & 0x000000FF) >> 0;
    return r_ / 255.f;
}
float color::get_g() const
{
    std::uint32_t g_ = (rgba & 0x0000FF00) >> 8;
    return g_ / 255.f;
}
float color::get_b() const
{
    std::uint32_t b_ = (rgba & 0x00FF0000) >> 16;
    return b_ / 255.f;
}
float color::get_a() const
{
    std::uint32_t a_ = (rgba & 0xFF000000) >> 24;
    return a_ / 255.f;
}

// set_color()
void color::set_r(const float r)
{
    std::uint32_t r_ = static_cast<std::uint32_t>(r * 255);
    rgba &= 0xFFFFFF00;
    rgba |= (r_ << 0);
}
void color::set_g(const float g)
{
    std::uint32_t g_ = static_cast<std::uint32_t>(g * 255);
    rgba &= 0xFFFF00FF;
    rgba |= (g_ << 8);
}
void color::set_b(const float b)
{
    std::uint32_t b_ = static_cast<std::uint32_t>(b * 255);
    rgba &= 0xFF00FFFF;
    rgba |= (b_ << 16);
}
void color::set_a(const float a)
{
    std::uint32_t a_ = static_cast<std::uint32_t>(a * 255);
    rgba &= 0x00FFFFFF;
    rgba |= (a_ << 24);
}

static void initialize_internal(std::string_view   title,
                                const window_mode& desired_window_mode)
{
    if (already_exist)
    {
        throw std::runtime_error("engine already initialized");
    }

    using namespace std;

    stringstream serr;

    SDL_version compiled = { 0, 0, 0 };
    SDL_version linked   = { 0, 0, 0 };

    SDL_VERSION(&compiled) SDL_GetVersion(&linked);

    if (SDL_COMPILEDVERSION !=
        SDL_VERSIONNUM(linked.major, linked.minor, linked.patch))
    {
        serr << "warning: SDL2 compiled and linked version mismatch: "
             << compiled << " " << linked << endl;
    }

    const int init_result = SDL_Init(SDL_INIT_EVERYTHING);
    if (init_result != 0)
    {
        const char* err_message = SDL_GetError();
        serr << "error: failed call SDL_Init: " << err_message << endl;
        throw std::runtime_error(serr.str());
    }

    ogl::log << "initialize internal" << std::endl;
    ogl::log << title << std::endl;
    if (std::string_view("Windows") == SDL_GetPlatform())
    {
        if (!cout)
        {
#ifdef _WIN32
            AllocConsole();
#endif
            if (nullptr == std::freopen("CON", "w", stdout))
            {
                throw std::runtime_error("can't reopen stdout");
            }
            std::cout.clear();
            std::cerr << "test" << std::endl;
        }
    }

#if defined(__ANDROID__)
    {
        SDL_DisplayMode disp_mode;
        SDL_GetCurrentDisplayMode(0, &disp_mode);
        current_window_mode.width  = disp_mode.w;
        current_window_mode.height = disp_mode.h;
        current_window_mode.scale  = current_window_mode.get_window_scale();
    }
#else
    current_window_mode.width  = static_cast<int>(desired_window_mode.width);
    current_window_mode.height = static_cast<int>(desired_window_mode.height);
    current_window_mode.scale  = current_window_mode.get_window_scale();
#endif

    window = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED, current_window_mode.width,
                              current_window_mode.height,
                              SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    if (window == nullptr)
    {
        const char* err_message = SDL_GetError();
        serr << "error: failed call SDL_CreateWindow: " << err_message << endl;
        SDL_Quit();
        throw std::runtime_error(serr.str());
    }

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

    gl_context = SDL_GL_CreateContext(window);
    if (gl_context == nullptr)
    {
        const char* err_message = SDL_GetError();
        serr << "error: can't create opengl context: " << err_message << endl;
        throw std::runtime_error(serr.str());
    }

    //  int swap_applyed = SDL_GL_SetSwapInterval(0);
    //  assert(swap_applyed == 0);

    int gl_major_ver = 0;
    int result =
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &gl_major_ver);
    assert(result == 0);
    int gl_minor_ver = 0;
    result = SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &gl_minor_ver);
    assert(result == 0);

    ogl::log << "gl_context: " << gl_major_ver << '.' << gl_minor_ver
             << std::endl;

    if (gl_major_ver < 3)
    {
        serr << "current context opengl version: " << gl_major_ver << '.'
             << gl_minor_ver << '\n'
             << "need opengl version at least: 3.0 es\n"
             << std::flush;
        throw std::runtime_error(serr.str());
    }
    try
    {
#ifndef __ANDROID__
#if defined(PFNGLGETERRORPROC)
        load_gl_func("glGetError", glGetError);
#endif
        load_gl_func("glCreateShader", glCreateShader);
        load_gl_func("glShaderSource", glShaderSource);
        load_gl_func("glCompileShader", glCompileShader);
        load_gl_func("glGetShaderiv", glGetShaderiv);
        load_gl_func("glGetShaderInfoLog", glGetShaderInfoLog);
        load_gl_func("glDeleteShader", glDeleteShader);
        load_gl_func("glCreateProgram", glCreateProgram);
        load_gl_func("glAttachShader", glAttachShader);
        load_gl_func("glBindAttribLocation", glBindAttribLocation);
        load_gl_func("glLinkProgram", glLinkProgram);
        load_gl_func("glGetProgramiv", glGetProgramiv);
        load_gl_func("glGetProgramInfoLog", glGetProgramInfoLog);
        load_gl_func("glDeleteProgram", glDeleteProgram);
        load_gl_func("glUseProgram", glUseProgram);
        load_gl_func("glVertexAttribPointer", glVertexAttribPointer);
        load_gl_func("glEnableVertexAttribArray", glEnableVertexAttribArray);
        load_gl_func("glDisableVertexAttribArray", glDisableVertexAttribArray);
        load_gl_func("glValidateProgram", glValidateProgram);
        load_gl_func("glGetUniformLocation", glGetUniformLocation);
        load_gl_func("glUniform1i", glUniform1i);
        load_gl_func("glUniform4fv", glUniform4fv);
        load_gl_func("glActiveTexture", glActiveTexture_);
        load_gl_func("glGenerateMipmap", glGenerateMipmap);
        load_gl_func("glUniformMatrix3fv", glUniformMatrix3fv);
        load_gl_func("glBufferSubData", glBufferSubData);
        load_gl_func("glBlendEquationSeparate", glBlendEquationSeparate);
        load_gl_func("glBlendFuncSeparate", glBlendFuncSeparate);
        load_gl_func("glGetAttribLocation", glGetAttribLocation);
        load_gl_func("glDeleteBuffers", glDeleteBuffers);
        load_gl_func("glDetachShader", glDetachShader);
        load_gl_func("glDeleteVertexArrays", glDeleteVertexArrays);
        load_gl_func("glBindBuffer", glBindBuffer);
        load_gl_func("glGenBuffers", glGenBuffers);
        load_gl_func("glGenVertexArrays", glGenVertexArrays);
        load_gl_func("glBindVertexArray", glBindVertexArray);
        load_gl_func("glBufferData", glBufferData);
#endif
#ifdef __ANDROID__
        load_gl_func("glGenVertexArraysOES", glGenVertexArrays);
        load_gl_func("glBindVertexArrayOES", glBindVertexArray);
        load_gl_func("glDeleteVertexArraysOES", glDeleteVertexArrays);
#endif
    }
    catch (std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
        throw;
    }

    glGenBuffers(1, &VBO);
    OMG_CHECK()
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    OMG_CHECK()
    GLuint data_size_in_bytes = 0;
    glBufferData(GL_ARRAY_BUFFER, data_size_in_bytes, nullptr, GL_STATIC_DRAW);
    OMG_CHECK()
    glBufferSubData(GL_ARRAY_BUFFER, 0, data_size_in_bytes, nullptr);
    OMG_CHECK()

    shader_simple = new shader_gl_es30(
        R"(#version 300 es
                                  uniform mat3 u_matrix;
        layout(location = 0)      in vec2 a_position;
        layout(location = 1)      in vec4 a_color;
        layout(location = 2)      in vec2 a_tex_coord;
                                  out vec4 v_color;
                                  out vec2 v_tex_coord;
                                  void main()
                                  {
                                      vec3 pos = vec3(a_position, 1.0) * u_matrix;
                                      gl_Position = vec4(pos, 1.0);
                                      v_tex_coord = a_tex_coord;
                                      v_color = a_color;
                                  }
                                  )",
        R"(#version 300 es
                                  #ifdef GL_ES
                                  precision mediump float;
                                  #endif

                                  in vec4 v_color;
                                  in vec2 v_tex_coord;
                                  out vec4 color;
                                  uniform sampler2D s_texture;
                                  void main()
                                  {
                                      color = texture(s_texture, v_tex_coord) * v_color;

                                  }
                                  )",
        { { 0, "a_position" }, { 1, "a_color" }, { 2, "a_tex_coord" } });

    shader_simple->use();

    shader_colored = new shader_gl_es30("res/shader_src/shader_colored.glsl");
    shader_colored->use();

    glEnable(GL_BLEND);
    OMG_CHECK()
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    OMG_CHECK()

    glClearColor(0.f, 0.f, 0.f, 0.f);
    OMG_CHECK()

    /// glViewport(0, 0, current_window_mode.width, current_window_mode.height);
    resize_window(current_window_mode.width, current_window_mode.height);
    OMG_CHECK()

    /// initialize audio -------------------------------------------------
    audio_device_spec.freq     = 48000;
    audio_device_spec.format   = AUDIO_S16LSB;
    audio_device_spec.channels = 2;
    audio_device_spec.samples  = 1024; // must be power of 2
    audio_device_spec.callback = audio_callback;
    audio_device_spec.userdata = nullptr;

    const int num_audio_drivers = SDL_GetNumAudioDrivers();
    for (int i = 0; i < num_audio_drivers; ++i)
    {
        std::cout << "audio_driver #:" << i << " " << SDL_GetAudioDriver(i)
                  << '\n';
    }
    std::cout << std::flush;

    // TODO on windows 10 only directsound - works for me
    if (std::string_view("Windows") == SDL_GetPlatform())
    {
        const char* selected_audio_driver = SDL_GetAudioDriver(1);
        ogl::log << "selected_audio_driver: " << selected_audio_driver
                 << std::endl;

        if (0 != SDL_AudioInit(selected_audio_driver))
        {
            std::cout << "can't init SDL audio\n" << std::flush;
        }
    }

    const char* default_audio_device_name = nullptr;

    // SDL_FALSE - mean get only OUTPUT audio devices
    const int num_audio_devices = SDL_GetNumAudioDevices(SDL_FALSE);
    if (num_audio_devices > 0)
    {
        default_audio_device_name = SDL_GetAudioDeviceName(0, SDL_FALSE);
        for (int i = 0; i < num_audio_devices; ++i)
        {
            ogl::log << "audio device #" << i << ": "
                     << SDL_GetAudioDeviceName(i, SDL_FALSE) << '\n';
        }
    }
    std::cout << std::flush;

    audio_device =
        SDL_OpenAudioDevice(default_audio_device_name, 0, &audio_device_spec,
                            nullptr, SDL_AUDIO_ALLOW_ANY_CHANGE);

    if (audio_device == 0)
    {
        std::cerr << "failed open audio device: " << SDL_GetError();
        throw std::runtime_error("audio failed");
    }
    else
    {
        ogl::log << "audio device selected: " << default_audio_device_name
                 << '\n'
                 << "freq: " << audio_device_spec.freq << '\n'
                 << "format: "
                 << get_sound_format_name(audio_device_spec.format) << '\n'
                 << "channels: "
                 << static_cast<uint32_t>(audio_device_spec.channels) << '\n'
                 << "samples: " << audio_device_spec.samples << '\n'
                 << std::flush;

        // unpause device
        SDL_PauseAudioDevice(audio_device, SDL_FALSE);
    }

    already_exist = true;
}

void audio_callback(void*, uint8_t* stream, int stream_size)
{
    // no sound default
    std::fill_n(stream, stream_size, 0);

    for (sound_buffer_impl* snd : sounds)
    {
        if (snd->is_playing_)
        {
            uint32_t rest         = snd->length - snd->current_index;
            uint8_t* current_buff = &snd->buffer[snd->current_index];

            if (rest <= static_cast<uint32_t>(stream_size))
            {
                // copy rest to buffer
                SDL_MixAudioFormat(stream, current_buff,
                                   audio_device_spec.format, rest,
                                   SDL_MIX_MAXVOLUME);
                snd->current_index += rest;
            }
            else
            {
                SDL_MixAudioFormat(
                    stream, current_buff, audio_device_spec.format,
                    static_cast<uint32_t>(stream_size), SDL_MIX_MAXVOLUME);
                snd->current_index += static_cast<uint32_t>(stream_size);
            }

            if (snd->current_index == snd->length)
            {
                if (snd->is_looped)
                {
                    // start from begining
                    snd->current_index = 0;
                }
                else
                {
                    snd->is_playing_ = false;
                }
            }
        }
    }
}

class android_redirected_buf : public std::streambuf
{
public:
    android_redirected_buf() = default;

private:
    // This android_redirected_buf buffer has no buffer. So every character
    // "overflows" and can be put directly into the teed buffers.
    int overflow(int c) override
    {
        if (c == EOF)
        {
            return !EOF;
        }
        else
        {
            if (c == '\n')
            {
#ifdef __ANDROID__
                // android log function add '\n' on every print itself
                __android_log_print(ANDROID_LOG_ERROR, "cheeky_log", "%s",
                                    message.c_str());
#else
                std::printf("%s\n", message.c_str()); // TODO test only
#endif
                message.clear();
            }
            else
            {
                message.push_back(static_cast<char>(c));
            }
            return c;
        }
    }

    int sync() override
    {
        return 0;
    }

    std::string message;
};

static auto cout_buf = std::cout.rdbuf();
static auto cerr_buf = std::cerr.rdbuf();
static auto clog_buf = std::clog.rdbuf();

android_redirected_buf logcat;

void initialize(std::string_view title, const window_mode& desired_window_mode)
{
#ifdef __ANDROID__
    std::cout.rdbuf(&logcat);
    std::cerr.rdbuf(&logcat);
    std::clog.rdbuf(&logcat);
#endif

    initialize_internal(title, desired_window_mode);
}

void unitialize()
{
    if (already_exist)
    {
#ifdef __ANDROID__
        // restore default buffer
        std::cout.rdbuf(cout_buf);
        std::cerr.rdbuf(cerr_buf);
        std::clog.rdbuf(clog_buf);
#endif

        glDeleteVertexArrays(1, &VAO);
        OMG_CHECK()
        glDeleteBuffers(1, &VBO);
        OMG_CHECK()

        delete shader_simple;
        delete shader_colored;
        SDL_GL_DeleteContext(gl_context);
        SDL_DestroyWindow(window);
        SDL_Quit();
        already_exist = false;
    }
}

} // namespace ogl
