#pragma once

#include <fstream>
#include <iosfwd>
#include <iostream>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>

#ifndef OGL_DECLSPEC
#define OGL_DECLSPEC
#endif

namespace ogl
{

struct OGL_DECLSPEC membuf : public std::streambuf
{
    membuf()
        : std::streambuf()
        , buf()
        , buf_size(0)
    {
    }
    membuf(std::unique_ptr<char[]> buffer, size_t size)
        : std::streambuf()
        , buf(std::move(buffer))
        , buf_size(size)
    {
        char* beg_ptr = buf.get();
        char* end_ptr = beg_ptr + buf_size;
        setg(beg_ptr, beg_ptr, end_ptr);
        setp(beg_ptr, end_ptr);
    }
    membuf(membuf&& other)
    {
        setp(nullptr, nullptr);
        setg(nullptr, nullptr, nullptr);

        other.swap(*this);

        buf      = std::move(other.buf);
        buf_size = other.buf_size;

        other.buf_size = 0;
    }

    pos_type seekoff(off_type pos, std::ios_base::seekdir seek_dir,
                     std::ios_base::openmode) override
    {
        // TODO implement it in correct way
        if (seek_dir == std::ios_base::beg)
        {
            return 0 + pos;
        }
        else if (seek_dir == std::ios_base::end)
        {
            return buf_size + pos;
        }
        else
        {
            return egptr() - gptr();
        }
    }

    char*  begin() const { return eback(); }
    size_t size() const { return buf_size; }

private:
    std::unique_ptr<char[]> buf;
    size_t                  buf_size;
};

OGL_DECLSPEC membuf load_file(std::string_view path);

OGL_DECLSPEC inline std::stringstream filter_comments(std::string_view file)
{
    std::stringstream out;
    std::string       line;
    ogl::membuf       buf = ogl::load_file(file);
    std::istream      in(&buf);

    if (!in)
    {
        std::cerr << "can't open file: " << file.data() << '\n';
        throw std::runtime_error(std::string("can't open file: ") +
                                 file.data());
    }

    while (std::getline(in, line))
    {
        std::size_t comment_pos = line.find("//");
        if (comment_pos != std::string::npos)
        {
            line = line.substr(0, comment_pos);
        }
        if (!line.empty())
        {
            out << line << '\n';
        }
    }

    return out;
}
} // namespace ogl
