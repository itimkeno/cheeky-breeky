#pragma once

#include <cmath>

#ifndef OGL_DECLSPEC
#define OGL_DECLSPEC
#endif

namespace ogl
{
// math

struct OGL_DECLSPEC vec2
{
    vec2();
    // vec2(const vec2&) = default;
    vec2(float x_, float y_);

    vec2& operator+=(const vec2& r);
    vec2& operator-=(const vec2& r);
    vec2& operator/(const float& r);
    vec2  operator-() const;
    float length() const;
    float length_sqr() const;
    vec2& rotate(float radians);
    vec2& normalize();

    float x = 0;
    float y = 0;
    // vec   normalize(const vec& r);
};

OGL_DECLSPEC vec2 operator*(const vec2& l, const vec2& r);
OGL_DECLSPEC vec2 operator*(const vec2& l, const float& r);
OGL_DECLSPEC vec2 operator*(const float& l, const vec2& r); // ??

OGL_DECLSPEC vec2 operator+(const vec2& l, const vec2& r);
OGL_DECLSPEC vec2 operator+(const vec2& l, const float& r);
OGL_DECLSPEC vec2 operator+(const float& l, const vec2& r);

OGL_DECLSPEC vec2 operator-(const vec2& l, const vec2& r);
OGL_DECLSPEC vec2 operator-(const vec2& l, const float& r);
OGL_DECLSPEC vec2 operator-(const float& l, const vec2& r);

OGL_DECLSPEC vec2  min(const vec2& l, const vec2& r);
OGL_DECLSPEC vec2  max(const vec2& l, const vec2& r);
OGL_DECLSPEC float dot(const vec2& l, const vec2& r);
OGL_DECLSPEC float dist_sqr(const vec2& l, const vec2& r);
OGL_DECLSPEC vec2  cross(const vec2& l, const float& r);
OGL_DECLSPEC vec2  cross(const float& l, const vec2& r);
OGL_DECLSPEC float cross(const vec2& l, const vec2& r);
OGL_DECLSPEC float distance(const vec2& l, const vec2& r);

//--------------------self impl---------------------------------
OGL_DECLSPEC bool  equal(const float& l, const float& r);
OGL_DECLSPEC float sqr(float a);
OGL_DECLSPEC float clamp(float& min, float& max, float& a);
OGL_DECLSPEC int   round(float& a);
OGL_DECLSPEC float random(float& l, float& r);
OGL_DECLSPEC bool  bias_grater_than(float& a, float& b);
//  loool
// --------------------------------------------------------------

struct OGL_DECLSPEC matrix3x2
{
    matrix3x2();
    matrix3x2(vec2 row0, vec2 row1, vec2 row2);

    static matrix3x2 identity();
    static matrix3x2 scale(float scale);
    static matrix3x2 scale(float scale_x, float scale_y);
    static matrix3x2 rotation(float thetha);
    static matrix3x2 move(const vec2& delta);
    vec2             axis_x() const;
    vec2             axis_y() const;
    matrix3x2        transponse() const;

    vec2 row0;
    vec2 row1;
    vec2 row2;
};

OGL_DECLSPEC vec2      operator*(const vec2& v, const matrix3x2& m);
OGL_DECLSPEC matrix3x2 operator*(const matrix3x2& m1, const matrix3x2& m2);

// math end
const float pi            = 3.141592741f;
const float epsilon       = 0.0001f;
const float gravity_scale = 5.0f;
const vec2  gravity(0, 10.0f * gravity_scale);
const float dt = 1.0f / 60.0f;

} // namespace ogl
