#pragma once

#include <chrono>
#include <iosfwd>
#include <memory>
#include <string>
#include <string_view>
#include <variant>

#ifndef OGL_DECLSPEC
#define OGL_DECLSPEC
#endif

#include "configuration_loader.hxx"
#include "math.hxx"

namespace ogl
{
// input -------------------------------------------------------
enum class event_type
{
    input_key,
    hardware,
    mouse_motion,
    mouse_button
};

enum class ekeys
{
    escape,
    tab,
    space,
    left,
    right,
    up,
    down,
    i,
    button_right,
    button_left
};

enum class mouse
{
    button_right,
    button_left

};
struct input_data_keyboard
{
    ogl::ekeys key;
    bool       is_down;
};

struct input_data_mouse
{
    ogl::mouse key_mouse;
    bool       is_down_button;
    bool       is_up_button;
    int        x; // x pos clik
    int        y; // y pos clik
};

struct mouse_motion_data
{
    bool moving;
    int  x; // x pos
    int  y; // y pos
    bool left_state;
    bool right_state;
};

struct hardware_data
{
    bool is_reset;
    bool is_resized;
};

struct event
{
    std::variant<input_data_keyboard, hardware_data, input_data_mouse,
                 mouse_motion_data>
               info;
    double     timestamp = 0;
    event_type type;
};

OGL_DECLSPEC std::ostream& operator<<(std::ostream& stream, const event e);
OGL_DECLSPEC std::ostream& operator<<(std::ostream& stream,
                                      const input_data_keyboard&);
OGL_DECLSPEC std::ostream& operator<<(std::ostream& stream,
                                      const input_data_mouse&);
OGL_DECLSPEC std::ostream& operator<<(std::ostream& stream,
                                      const mouse_motion_data&);
OGL_DECLSPEC std::ostream& operator<<(std::ostream& stream,
                                      const hardware_data&);
// input end --------------------------------------------------------

class OGL_DECLSPEC color
{
public:
    color() = default;
    explicit color(std::uint32_t rgba_);
    color(float r, float g, float b, float a);

    float get_r() const;
    float get_g() const;
    float get_b() const;
    float get_a() const;

    void set_r(const float r);
    void set_g(const float g);
    void set_b(const float b);
    void set_a(const float a);

private:
    std::uint32_t rgba = 0;
};

// position & color & texture
struct OGL_DECLSPEC vertex
{
    vec2  pos;
    vec2  uv;
    color c;
};

// >>
OGL_DECLSPEC std::istream& operator>>(std::istream& is, vec2&);
OGL_DECLSPEC std::istream& operator>>(std::istream& is, matrix3x2&);
OGL_DECLSPEC std::istream& operator>>(std::istream& is, color&);
OGL_DECLSPEC std::istream& operator>>(std::istream& is, vertex&);
// >> end
class OGL_DECLSPEC texture
{
public:
    virtual ~texture();
    virtual std::uint32_t get_width() const  = 0;
    virtual std::uint32_t get_height() const = 0;
    virtual std::string   get_name() const   = 0;
};

class OGL_DECLSPEC vertex_buffer // vbo
{
public:
    virtual ~vertex_buffer();
    // virtual const vertex* data() const = 0;
    virtual std::size_t size() const = 0;
    virtual void        bind() const = 0;
};

class OGL_DECLSPEC sound_buffer
{
public:
    enum class properties
    {
        once,
        looped
    };

    virtual ~sound_buffer();
    virtual void play(const properties) = 0;
    virtual bool is_playing() const     = 0;
    virtual void stop()                 = 0;
};

struct OGL_DECLSPEC window_mode
{                            //  need fix that
    int   width         = 0; // in game.cxx | screen_width  = 1280.f;
    int   height        = 0; // in game.cxx | screen_height  = 720.f;
    float scale         = 0;
    bool  is_fullscreen = false;

    float       get_window_scale();
    window_mode get_current_window_mode();
    void        set_current_window_mode(const int width_, const int height_);
};

OGL_DECLSPEC void initialize(std::string_view   title,
                             const window_mode& desired_window_mode);

OGL_DECLSPEC vec2 get_current_window_size();

OGL_DECLSPEC float get_time_from_init();

OGL_DECLSPEC bool read_event(event& e);

OGL_DECLSPEC bool is_key_down(const ekeys);

OGL_DECLSPEC texture* create_texture(std::string_view path);
OGL_DECLSPEC void     destroy_texture(texture* t);

OGL_DECLSPEC vertex_buffer* create_vertex_buffer(const vertex*, std::size_t);
OGL_DECLSPEC void           destroy_vertex_buffer(vertex_buffer*);

OGL_DECLSPEC sound_buffer* create_sound_buffer(std::string_view path);
OGL_DECLSPEC void          destroy_sound_buffer(sound_buffer*);

enum class primitives
{
    lines,
    line_strip,
    line_loop,
    triangls,
    trianglestrip,
    trianglfan
};

OGL_DECLSPEC void render(const primitives     primitive_type,
                         const vertex_buffer& buff, const texture* tex,
                         const matrix3x2& m);
OGL_DECLSPEC void render_color(const primitives     primitive_type,
                               const vertex_buffer& buff, const texture* tex,
                               const matrix3x2& m, const color& col);

OGL_DECLSPEC void unitialize();
OGL_DECLSPEC void swap_buffers();

extern OGL_DECLSPEC std::ostream& log;

} // namespace ogl

// extern std::unique_ptr<ogl::any_game> create_game();
