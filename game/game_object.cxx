#include "game_object.hxx"
// #include <engine/configuration_loader.hxx>

std::istream& operator>>(std::istream& stream, game_object& obj)
{
    std::string start_word;
    stream >> start_word;
    if (start_word != "object")
    {
        throw std::runtime_error("can't parse game object got: " + start_word);
    }

    stream >> obj.name;
    stream >> obj.type;

    float direction_in_grad = 0.0f;
    stream >> direction_in_grad;
    obj.rotation = direction_in_grad * (ogl::pi / 180.f);

    stream >> obj.position;
    stream >> obj.size;

    std::string key_word;
    stream >> key_word;
    if (key_word != "mesh")
    {
        throw std::runtime_error("can't parse game object mesh got: " +
                                 key_word);
    }

    stream >> obj.path_mesh;
    ogl::vertex_buffer* mesh =
        load_mesh_from_file_with_scale(obj.path_mesh, obj.size);
    assert(mesh != nullptr);
    obj.mesh = mesh;

    stream >> key_word;
    if (key_word != "texture")
    {
        throw std::runtime_error("can't parse game object texture got: " +
                                 key_word);
    }

    stream >> obj.path_texture;
    ogl::texture* tex = ogl::create_texture(obj.path_texture);
    assert(tex != nullptr);
    obj.texture = tex;

    return stream;
}

ogl::vertex_buffer* load_mesh_from_file_with_scale(const std::string_view& path,
                                                   const ogl::vec2& scale)
{
    std::stringstream file = ogl::filter_comments(path);
    if (!file)
    {
        throw std::runtime_error("can't load identity_quad.txt");
    }

    std::size_t num_of_vertexes = 0;
    std::string num_of_ver;

    file >> num_of_ver;

    if (num_of_ver != "num_of_vertexes")
    {
        throw std::runtime_error("no key word: num_of_vertexes");
    }

    file >> num_of_vertexes;

    std::vector<ogl::vertex> vertexes;

    vertexes.reserve(num_of_vertexes);

    std::copy_n(std::istream_iterator<ogl::vertex>(file), num_of_vertexes,
                std::back_inserter(vertexes));

    ogl::matrix3x2 scale_mat = ogl::matrix3x2::scale(scale.x, scale.y);

    std::transform(begin(vertexes), end(vertexes), begin(vertexes),
                   [&scale_mat](ogl::vertex v)
                   {
                       v.pos = v.pos * scale_mat;
                       return v;
                   });

    ogl::vertex_buffer* vert_buff =
        ogl::create_vertex_buffer(vertexes.data(), num_of_vertexes);
    return vert_buff;
}
