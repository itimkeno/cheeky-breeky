#pragma once

#include "game_object.hxx"
#include <engine/math.hxx>

#include <list>
#include <vector>

struct checkers_all
{
    struct checker
    {
        ogl::vec2   pos{ 0.f, 0.f };
        float       radius = 0.f;
        ogl::vec2   velocity{ 0.f, 0.f };
        float       angular_velocity = 0.f;
        float       torque           = 0.f; // krut moment
        float       dir              = 0.f;
        ogl::vec2   force{ 0.f, 0.f };
        float       mass             = 0.f;
        float       inv_mass         = 0.f;
        float       inertia          = 0.f;
        float       inv_inertia      = 0.f;
        float       friction_static  = 0.f;
        float       friction_dynamic = 0.f;
        float       restitution      = 0.f;
        bool        is_alive_        = true;
        bool        selected         = false;
        game_object game_obj;

        ogl::vec2 pos_prev = { 0.f, 0.f };

        void apply_force(const ogl::vec2& f);
        void apply_impulse(const ogl::vec2& impulse,
                           const ogl::vec2& contact_vector);
        void set_static();
        void set_dir(float radians);
    };

    std::vector<checker>            checkers;
    std::vector<game_object*>       render_list_;
    const std::vector<game_object>& sprites_;
    // std::vector<psc::manifold> conacts;

    float       friction_deck    = 0.8f;
    float       power            = 5.f;
    std::size_t is_alive_count   = 0;
    std::size_t dead_count_white = 0;
    std::size_t dead_count_black = 0;
    std::size_t user_white       = 4;
    // float                           step_timer       = 1.0f;
    // float                           delta_step       = 5.0f;

    float world_size_x = 0;
    float world_size_y = 0;

    explicit checkers_all(const std::vector<game_object>& sprites);

    void                             update(float dt);
    const std::vector<game_object*>& render_list() const;

    void touch_create(const ogl::vec2& finger_pos);
    void touch_and_move(const ogl::vec2& finger_pos);
    void touch_delete();

    void slingshoot_activate(const ogl::vec2& finger_pos);
    // void slingshoot_calculate(const ogl::vec2& finger_pos);
    void slingshoot_shoot(const ogl::vec2& finger_pos);

private:
    void integrate_forces(checker& head, const float& dt);
    void integrate_velocity(checker& head, const float& dt);

    void      is_alive_user();
    float     get_next_direction(const checker& head);
    ogl::vec2 apply_friction(const checker& head);
    void      update_physics(float dt);
    void      make_edge_wall();
};
