#include "game.hxx"
#include <string>

#ifdef __ANDROID__
extern "C" int SDL_main();
#endif

int main()
{
    try
    {
        raptor_game game;
        return game.on_main_loop();
    }
    catch (std::exception& ex)
    {
        std::cerr << ex.what() << std::endl;
    }
    return EXIT_FAILURE;
}

#ifdef __ANDROID__
int SDL_main()
{
    return main();
}
#endif
