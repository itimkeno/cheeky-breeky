#include "checkers.hxx"
#include <physics/physics.hxx>

#include <array>
#include <cmath>
#include <iostream>
#include <iterator>

// constexpr float                   one_degree = 0.0175f;
static std::vector<psc::manifold> contacts;
static std::size_t                m_iterations = 10;

checkers_all::checkers_all(const std::vector<game_object>& sprites)
    : sprites_(sprites)
{
    auto gen_checker = [&](unsigned game_object_index)
    {
        checker head;
        head.game_obj         = sprites_.at(game_object_index);
        head.dir              = head.game_obj.rotation;
        head.pos              = head.game_obj.position;
        head.radius           = head.game_obj.size.x / 2; // only circle object
        head.friction_static  = 1.5f;
        head.friction_dynamic = 0.5f;
        head.restitution      = 0.2f;
        // fix mass and density to one coordinate system
        head.mass = ogl::pi * head.radius * head.radius * 0.5f; // 1.0f density
        head.inv_mass    = 1.0f / head.mass;
        head.inertia     = head.mass * head.radius * head.radius;
        head.inv_inertia = 1.0f / head.inertia;

        head.angular_velocity = 0.f;
        head.torque           = 0.f;
        head.is_alive_        = true;
        head.selected         = true;

        return head;
    };

    for (unsigned index = 1; index < sprites_.size(); index++)
    {
        checker generated = gen_checker(index);
        checkers.push_back(generated);
    }
}

void checkers_all::checker::set_dir(float radians)
{
    dir                     = radians;
    this->game_obj.rotation = radians;
}

float checkers_all::get_next_direction(const checker& head)
{
    return head.dir;
}

void checkers_all::touch_create(const ogl::vec2& finger_pos)
{
    checker touch;
    touch.game_obj.type     = object_type::touch;
    touch.pos               = finger_pos;
    touch.game_obj.position = touch.pos;
    touch.velocity          = { 0.f, 0.f };
    touch.dir               = 0.f;
    touch.radius            = 15.f;
    touch.game_obj.size     = { 30.f, 30.f };
    touch.friction_static   = 1.5f;
    touch.friction_dynamic  = 0.5f;
    touch.restitution       = 0.2f;
    touch.mass        = ogl::pi * touch.radius * touch.radius * 1.0f; // density
    touch.inv_mass    = 1.0f / touch.mass;
    touch.inertia     = touch.mass * touch.radius * touch.radius;
    touch.inv_inertia = 1.0f / touch.inertia;

    touch.angular_velocity = 0.f;
    touch.torque           = 0.f;
    touch.is_alive_        = true;
    touch.selected         = true;

    checkers.push_back(touch);
}

void checkers_all::touch_delete()
{
    auto it = std::find_if(begin(checkers), end(checkers),
                           [](const checker& touch) {
                               return touch.game_obj.type == object_type::touch;
                           });

    if (it != end(checkers))
    {
        checkers.erase(it);
    }
}

void checkers_all::touch_and_move(const ogl::vec2& finger_pos)
{
    for (checker& touch : checkers)
    {
        if (touch.game_obj.type == object_type::touch)
        {
            touch.pos               = finger_pos;
            touch.game_obj.position = touch.pos;
            touch.velocity =
                -1.f * ((touch.pos_prev - touch.pos) / ogl::dt) * 0.7f;

#ifndef NDEBUG
            ogl::log << "    " << touch.pos.x << ' ' << touch.pos.y
                     << std::endl;
#endif // DEBUG

            touch.pos_prev = touch.pos;
        }
    }
}

void checkers_all::slingshoot_activate(const ogl::vec2& finger_pos)
{
    for (checker& head : checkers)
    {
        if ((ogl::distance(head.pos, finger_pos) < head.radius) &&
            (head.game_obj.type == object_type::white_checkers))
        {
            head.selected = true;
        }
    }
}

void checkers_all::slingshoot_shoot(const ogl::vec2& touch)
{
    for (checker& head : checkers)
    {
        if (head.selected == true)
        {
            head.velocity += (head.pos - touch) * power;
            head.selected = false;
        }
    }
}

void checkers_all::update(float dt)
{
    render_list_.clear();

    checkers[18].set_static();
    checkers[12].set_static();
    checkers[10].set_static();
    checkers[15].set_static();

    update_physics(dt);

    // make_edge_wall();
    // is_alive_user();

    is_alive_count = 0;
    for (auto& checker : checkers)
    {
        if (checker.is_alive_)
            ++is_alive_count;
        // need ptr, because perfomance on the bootom
        checker.game_obj.position = checker.pos;
        checker.game_obj.rotation = checker.dir;
        render_list_.push_back(&checker.game_obj);
        // render_list_.resize(is_alive_count);
    }

    if (dead_count_white >= 9)
        ogl::log << "black wins" << std::endl;

    if (dead_count_black >= 9)
        ogl::log << "white wins" << std::endl;
}

const std::vector<game_object*>& checkers_all::render_list() const
{
    return render_list_;
}

void checkers_all::is_alive_user()
{
    // todo count black/white defeated checkers
    for (checker& head : checkers)
    {
        if (head.pos.x > 500 || head.pos.y > 500 || head.pos.x < 0 ||
            head.pos.y < 0)
        {
            head.is_alive_ = false;
        }
    }
}

void checkers_all::make_edge_wall()
{
    for (checker& head : checkers)
    {
        if (head.pos.x > world_size_x - head.radius)
        {
            head.pos.x      = world_size_x - head.radius;
            head.velocity.x = -head.velocity.x;
        }
        if (head.pos.x < 0 + head.radius)
        {
            head.pos.x      = 0.1f + head.radius;
            head.velocity.x = -head.velocity.x;
        }
        if (head.pos.y > world_size_y - head.radius)
        {
            head.pos.y      = world_size_y - head.radius;
            head.velocity.y = -head.velocity.y;
        }
        if (head.pos.y < 0 + head.radius)
        {
            head.pos.y      = 0.1f + head.radius;
            head.velocity.y = -head.velocity.y;
        }
    }
}

// -----------------------------------------------------------------------

void checkers_all::update_physics(float dt)
{
    contacts.clear();
    for (std::size_t i = 0; i < checkers.size(); ++i)
    {
        checker& a = checkers[i];
        for (std::size_t j = i + 1; j < checkers.size(); ++j)
        {
            checker& b = checkers[j];
            if (a.inv_mass == 0.f && b.inv_mass == 0.f)
                continue;
            psc::manifold m(&a, &b);
            m.solve_collision(); // check collision by game_object:: type
            if (m.contact_count)
                contacts.emplace_back(m);
        }
    }
    // integrate forces
    for (checker& i : checkers)
    {
        integrate_forces(i, dt);
    }
    // collision init
    for (psc::manifold& i : contacts)
    {
        i.initialize(dt);
    }
    // collision response
    for (std::size_t i = 0; i < m_iterations; ++i)
    {
        for (psc::manifold& j : contacts)
        {
            j.apply_impulse();
        }
    }
    // integrate velocity
    for (checker& i : checkers)
    {
        integrate_velocity(i, dt);
    }
    // correct position
    for (psc::manifold& i : contacts)
    {
        i.position_correction();
    }

    for (checker& i : checkers)
    {
        checker& b = i;
        b.force    = { 0.f, 0.f };
        b.torque   = 0.f;
    }
}

void checkers_all::integrate_forces(checker& head, const float& dt)
{
    if (head.inv_mass == 0.f)
        return;
    // /*+ ogl::gravity*/
    //    head.velocity +=
    //        (head.force * head.inv_mass - apply_friction(head)) * (dt / 2.0f);

    head.force = -head.velocity * (friction_deck + head.inv_mass);
    head.velocity += head.force * (dt / 2.0f);
    float stop_rotate = -head.angular_velocity * friction_deck * 0.01f;
    head.angular_velocity +=
        stop_rotate + (head.torque * head.inv_inertia) * (dt / 2.0f);
}

void checkers_all::integrate_velocity(checker& head, const float& dt)
{
    if (head.inv_mass == 0.f)
        return;

    head.pos += head.velocity * dt;
    head.dir += head.angular_velocity * dt;
    head.set_dir(head.dir);
    integrate_forces(head, dt);
}

// its difficult and wrong way
// need rewrite accelerate
ogl::vec2 checkers_all::apply_friction(const checker& head)
{
    float sum_velocity = head.velocity.x + head.velocity.y;
    if (sum_velocity == 0.f)
        return { 0.f, 0.f };
    float     df = friction_deck / sum_velocity;
    ogl::vec2 vf;
    vf.x = head.velocity.x - df * std::sin(head.dir);
    vf.y = head.velocity.y - df * std::cos(head.dir);
    return vf;
}

// to body

void checkers_all::checker::apply_force(const ogl::vec2& f)
{
    force += f;
}

void checkers_all::checker::apply_impulse(const ogl::vec2& impulse,
                                          const ogl::vec2& contact_vector)
{
    velocity += inv_mass * impulse;
    angular_velocity += inv_inertia * cross(contact_vector, impulse);
}

void checkers_all::checker::set_static()
{
    inertia     = 0.f;
    inv_inertia = 0.f;
    mass        = 0.f;
    inv_mass    = 0.f;
}
