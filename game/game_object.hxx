#pragma once

#include <engine/engine.hxx>

#include <algorithm>
#include <cassert>
#include <iostream>
#include <iterator>
#include <map>
#include <sstream>
#include <string>
#include <vector>

enum class object_type
{
    level,          // main background
    black_checkers, // oponent // circle
    white_checkers, // self // circle
    touch,          // transparent // circle
    cirlce,
    AABB,
    polygon
};

struct game_object
{
    std::string      name;
    enum object_type type;
    float            rotation;
    ogl::vec2        position;
    ogl::vec2        size;
    std::string      path_mesh;
    std::string      path_texture;

    ogl::vertex_buffer* mesh    = nullptr;
    ogl::texture*       texture = nullptr;
};

ogl::vertex_buffer* load_mesh_from_file_with_scale(const std::string_view& path,
                                                   const ogl::vec2& scale);

std::istream& operator>>(std::istream& stream, game_object& obj);

inline std::istream& operator>>(std::istream& stream, object_type& type);
std::istream&        operator>>(std::istream& stream, object_type& type)
{
    static const std::map<std::string, object_type> types = {
        { "level", object_type::level },
        { "black_checkers", object_type::black_checkers },
        { "white_checkers", object_type::white_checkers },
        { "touch", object_type::touch }
    };

    std::string type_name;
    stream >> type_name;

    auto it = types.find(type_name);

    if (it != end(types))
    {
        type = it->second;
    }
    else
    {
        std::stringstream ss;
        ss << "expected one of: ";
        std::for_each(begin(types), end(types),
                      [&ss](const auto& kv) { ss << kv.first << ", "; });
        ss << " but got: " << type_name;
        throw std::runtime_error(ss.str());
    }

    return stream;
}
