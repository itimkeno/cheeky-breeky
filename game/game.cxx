﻿#include "game.hxx"
// #include <engine/configuration_loader.hxx>

static int   screen_width  = 600;
static int   screen_height = 800;
static float screen_scale  = 0;
static float world_size_x  = 0;
static float world_size_y  = 0;

static ogl::texture* debug_texture = nullptr;
// static ogl::sound_buffer*    debug_sound   = nullptr;

raptor_game::~raptor_game()
{
    for (auto& snd : sound)
    {
        ogl::destroy_sound_buffer(snd.second);
    }
    for (auto& tex : textures)
    {
        ogl::destroy_texture(tex.second);
    }
    for (auto& obj : objects)
    {
        ogl::destroy_texture(obj.texture);
        ogl::destroy_vertex_buffer(obj.mesh);
    }
}

[[noreturn]] void raptor_game::exit(int return_code)
{
    ogl::unitialize();
    continue_game_loop = false;
    exit_code          = return_code;
    std::exit(return_code);
}

std::unique_ptr<raptor_game> create_game()
{
    ogl::log << "initialize engine" << std::endl;

    ogl::window_mode window_mode = { screen_width, screen_height, false };
    ogl::initialize("Cheeky-breeky", window_mode);

    ogl::log << "creating main game object..." << std::endl;
    auto game = std::make_unique<raptor_game>();
    ogl::log << "finish creating main game object" << std::endl;
    return game;
}

void raptor_game::on_initialize()
{
    ogl::texture* t_reddot = ogl::create_texture("res/texture/debug.png");
    std::pair     dbg_texture("debug_texture", t_reddot);
    textures.emplace(dbg_texture);

    ///////
    // debug_texture = textures.at("debug_texture");
    ///////

    ogl::sound_buffer* s_hanger =
        ogl::create_sound_buffer("res/sound/hanger.wav");
    std::pair debug_sound("debug_sound", s_hanger);
    sound.emplace(debug_sound);

    auto level = ogl::filter_comments("res/level_01.txt");

    std::string num_of_objects;
    level >> num_of_objects;

    if (num_of_objects != "num_of_objects")
    {
        throw std::runtime_error("no num_of_objects in level file");
    }

    std::size_t objects_num = 0;

    level >> objects_num;

    std::copy_n(std::istream_iterator<game_object>(level), objects_num,
                std::back_inserter(objects));

    checkers_.reset(new ::checkers_all(objects));
    checkers_->update(0);

    auto it_level = std::find_if(objects.begin(), objects.end(),
                                 [](const game_object& obj)
                                 { return obj.type == object_type::level; });

    if (it_level != objects.end())
    {
        world_size_x            = it_level->size.x;
        world_size_y            = it_level->size.y;
        checkers_->world_size_x = world_size_x;
        checkers_->world_size_y = world_size_y;
    }

    window_mode_  = window_mode_.get_current_window_mode();
    screen_width  = window_mode_.width;
    screen_height = window_mode_.height;
    screen_scale  = window_mode_.scale;

    // white_.reset(new ::white(objects));
    // white_->update(0);

    // black_.reset(new black());
    // black_->sprite = objects.at(1);
    ogl::log << "initialize is OK\n" << std::flush;
}

void raptor_game::on_event(ogl::event& event)
{
    if (debug_mode)
        std::cout << event << std::endl; // ostream event

    switch (event.type)
    {
        case ogl::event_type::hardware:
            if (std::get<ogl::hardware_data>(event.info).is_reset)
            {
                exit(EXIT_SUCCESS);
            }
            if (std::get<ogl::hardware_data>(event.info).is_resized)
            {

                window_mode_  = window_mode_.get_current_window_mode();
                screen_width  = window_mode_.width;
                screen_height = window_mode_.height;
                screen_scale  = window_mode_.scale;
            }
            break;

        case ogl::event_type::input_key:
        {
            const auto& key_data =
                std::get<ogl::input_data_keyboard>(event.info);
            if (key_data.is_down)
            {
                if (key_data.key == ogl::ekeys::space)
                {
                    sound.at("debug_sound")
                        ->play(ogl::sound_buffer::properties::once);
                }
                else if (key_data.key == ogl::ekeys::tab)
                {
                    if (!debug_mode)
                        std::cout << "event info:\n";
                    if (debug_mode)
                        std::cout << "event info OFF\n" << std::flush;

                    debug_mode = !debug_mode;
                }
                else if (key_data.key == ogl::ekeys::i)
                {
                    sound.at("debug_sound")->stop();
                    // imgui_view = !imgui_view;
                }
                else if (key_data.key == ogl::ekeys::escape)
                {
                    exit(EXIT_SUCCESS);
                }
            }
        }
        break;

        case ogl::event_type::mouse_button:
        {
            const auto& button_data =
                std::get<ogl::input_data_mouse>(event.info);
            if (button_data.is_down_button)
            {
                if (button_data.key_mouse == ogl::mouse::button_left)
                {
                    ogl::vec2 tmp = correct_mouse(button_data.x, button_data.y);
                    checkers_->slingshoot_activate(tmp);
                }
                if (button_data.key_mouse == ogl::mouse::button_right)
                {
                    ogl::vec2 tmp = correct_mouse(button_data.x, button_data.y);
                    checkers_->touch_create(tmp);
                }
            }
            if (button_data.is_up_button)
            {
                if (button_data.key_mouse == ogl::mouse::button_left)
                {
                    ogl::vec2 tmp = correct_mouse(button_data.x, button_data.y);
                    checkers_->slingshoot_shoot(tmp);
                }
                if (button_data.key_mouse == ogl::mouse::button_right)
                {
                    checkers_->touch_delete();
                }
            }
        }
        break;

        case ogl::event_type::mouse_motion:
        {
            const auto& mouse_data =
                std::get<ogl::mouse_motion_data>(event.info);
            if (mouse_data.moving && mouse_data.left_state)
            {
            }
            if (mouse_data.moving && mouse_data.right_state)
            {
                ogl::vec2 tmp = correct_mouse(mouse_data.x, mouse_data.y);
                checkers_->touch_and_move(tmp);
            }
        }
        break;
    }
}

void raptor_game::on_update(std::chrono::milliseconds frame_delta)
{
    if (checkers_)
    {
        float dt = static_cast<float>(frame_delta.count()) * 0.001f;
        checkers_->update(dt);
    }

    if (debug_mode)
        ogl::log << "fps: " << 1 / dt << std::endl;
}

void raptor_game::on_render() const
{
    struct draw
    {
        draw(const object_type type, const ogl::vec2& world_size)
            : obj_type(type)
            , world(ogl::matrix3x2::scale(2.f / world_size.x,
                                          -2.f / world_size.y) *
                    ogl::matrix3x2::move(ogl::vec2(-1.f, 1.f)))
        {
            /// screen_scale

            // let the world is rectangle 100x100 units with center in (50,
            // 50) build world matrix to map 100x100 X(0 to 100) Y(0 to 100)
            // to NDC X(-1 to 1) Y(-1 to 1) 2x2
            // but we see rect with aspect and correct by height
            // so field not 2x2 but aspect_height x aspect_height
        }

        void operator()(const game_object& obj) const
        {
            if (obj_type == obj.type)
            {
                //#ifdef __ANDROID__
                // TODO implement for rotate screen
                ogl::matrix3x2 aspect = ogl::matrix3x2::scale(
                    1.f, static_cast<float>(screen_width) /
                             static_cast<float>(screen_height));
                /*#else
                                ogl::matrix3x2 aspect =
                                    ogl::matrix3x2::scale(static_cast<float>(screen_height)
                / static_cast<float>(screen_width), 1); #endif*/
                ogl::matrix3x2 move = ogl::matrix3x2::move(obj.position);
                ogl::matrix3x2 rot  = ogl::matrix3x2::rotation(obj.rotation);

                ogl::matrix3x2 tmp = rot * move * world;
                ogl::matrix3x2 m   = tmp * aspect;

                ogl::vertex_buffer& vert_buff = *obj.mesh;
                ogl::texture*       texture   = obj.texture;

                ogl::render(ogl::primitives::triangls, vert_buff, texture, m);

                if (debug_texture)
                {
                    ogl::render(ogl::primitives::line_loop, vert_buff,
                                debug_texture, m);
                }
            }
        }
        const object_type    obj_type;
        const ogl::matrix3x2 world;
    };

    static const std::vector<object_type> render_order = { object_type::level };

    auto it = std::find_if(begin(objects), end(objects),
                           [](const game_object& obj)
                           { return obj.type == object_type::level; });

    if (it == end(objects))
    {
        throw std::runtime_error("no level object");
    }

    const ogl::vec2 world_size = it->size;

    std::for_each(begin(render_order), end(render_order),
                  [&](object_type type) {
                      std::for_each(begin(objects), end(objects),
                                    draw(type, world_size));
                  });

    if (checkers_)
    {
        const std::vector<game_object*>& render_list = checkers_->render_list();
        for (game_object* obj : render_list)
        {
            draw renderer_white(object_type::white_checkers, world_size);
            draw renderer_black(object_type::black_checkers, world_size);
            renderer_white(*obj);
            renderer_black(*obj);
        }
    }
}

// TODO: need optimaze with matrix scale
ogl::vec2 correct_mouse(const int x_mouse, const int y_mouse)
{
    float correct_x =
        static_cast<float>(world_size_x) / static_cast<float>(screen_width);
    float correct_y =
        static_cast<float>(world_size_y) / static_cast<float>(screen_height);

    float x_pos = 0.f;
    float y_pos = 0.f;

    auto shift_x = 0.f;
    auto shift_y = 0.f;

    // clamp screen_width  ->> world_size
    if (screen_width > screen_height)
    {
        shift_x = (static_cast<float>(screen_width - screen_height) / 2.f);
        x_pos   = (static_cast<float>(x_mouse) - shift_x) * correct_y;
        y_pos   = static_cast<float>(y_mouse) * correct_y;
    }

    // clamp screen_height  ->> world_size
    if (screen_height > screen_width)
    {
        shift_y = (static_cast<float>(screen_height - screen_width) / 2.f);
        y_pos   = (static_cast<float>(y_mouse) - shift_y) * correct_x;
        x_pos   = static_cast<float>(x_mouse) * correct_x;
    }

#ifndef NDEBUG
    ogl::log << "mouse_x: " << x_pos << "   |   mouse_y: " << y_pos;
#endif // DEBUG

    return { x_pos, y_pos };
}

int raptor_game::on_main_loop()
{
    struct start_guard
    {
        start_guard() {}
        ~start_guard()
        {
            ogl::log << "guard destructor" << std::endl;
            ogl::unitialize();
        }
    } guard;

    // initialize internal

    using clock_timer = std::chrono::high_resolution_clock;
    using nano_sec    = std::chrono::nanoseconds;
    using milli_sec   = std::chrono::milliseconds;
    using time_point  = std::chrono::time_point<clock_timer, nano_sec>;

    clock_timer timer;

    time_point start = timer.now();

    std::unique_ptr<raptor_game> rap_game = create_game();

    rap_game->on_initialize();

    while (continue_game_loop)
    {
        time_point end_last_frame = timer.now();
        ogl::event event;

        while (read_event(event))
        {
            rap_game->on_event(event);
            // problem whith high dpi mouse
        }

        milli_sec frame_delta =
            std::chrono::duration_cast<milli_sec>(end_last_frame - start);

        if (frame_delta.count() < 15) // 1000 % 60 = 16.666
        {
            std::this_thread::yield(); // too fast, give other apps CPU time
            continue;                  // wait till more time
        }

        rap_game->on_update(frame_delta);
        rap_game->on_render();

        ogl::swap_buffers();
        start = end_last_frame;
    }
    return exit_code;
}
