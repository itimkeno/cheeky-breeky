#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <string_view>
#include <thread>
#include <vector>

#include <engine/engine.hxx>
#include <physics/physics.hxx>

#include "checkers.hxx"

class raptor_game
{
public:
    raptor_game() = default;
    ~raptor_game();

    void on_initialize();
    void on_event(ogl::event&);
    void on_update(std::chrono::milliseconds frame_delta);
    void on_render() const;
    int  on_main_loop();

private:
    [[noreturn]] void exit(int return_code);

public:
    bool    debug_mode         = false;
    bool    continue_game_loop = true;
    int32_t exit_code          = EXIT_FAILURE;

private:
    std::vector<game_object>                  objects;
    std::map<std::string, ogl::texture*>      textures;
    std::map<std::string, ogl::sound_buffer*> sound;

    // std::map<std::string, ogl::vertex_buffer*> meshes;

    std::unique_ptr<checkers_all> checkers_;
    std::unique_ptr<raptor_game>  game;

    ogl::window_mode window_mode_;
};

ogl::vertex_buffer* load_mesh_from_file_with_scale(const std::string_view& path,
                                                   const ogl::vec2& scale);
ogl::vec2           correct_mouse(const int x_mouse, const int y_mouse);
